# -*- coding: utf-8 -*-
#############################################################################
#
#    CDM Technologies .,Ltd
#
#    Copyright (C) 2019-TODAY CDM Technologies(<https://www.cdmteck.com>)
#    Author: CDM Technologies(<https://www.cdmteck.com>)
#
#    You can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
#############################################################################

{
    'name': 'CDM Backend Theme',
    'author': "CDM Technologies",
    'version': '1.0',
    'category': 'CDM',
    'description': """
                    CDM Backend theme for Odoo 13.0 community edition
                   """,
    'depends': ['web', 'cdm_core'],
    'data': [
        'data/res.lang.csv',
        'data/res_partner_data.xml',
        'data/config_parameter.xml',
        'views/assets.xml',
        'views/res_config_settings_views.xml',
        'views/login_views.xml'
    ],
    'qweb': [
        'static/src/xml/field_status_bar.xml',
        'static/src/xml/action_manager.xml',
        'static/src/xml/sidebar_menu.xml',
        'static/src/xml/header.xml',
        'static/src/xml/menu.xml',
        'static/src/xml/control_panel.xml',
        'static/src/xml/dialog.xml',
        'static/src/xml/list.xml',
        'static/src/xml/form.xml',
        'static/src/xml/kanban.xml',
    ],
    'installable': True,
    'auto_install': False,
}
