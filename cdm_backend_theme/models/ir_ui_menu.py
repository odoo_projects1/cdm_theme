# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import api, models, tools


class IrUiMenu(models.Model):
    _inherit = 'ir.ui.menu'

    @api.model
    @tools.ormcache_context('self._uid', 'debug', keys=('lang',))
    def load_menus(self, debug):
        menu_root = super(IrUiMenu, self).load_menus(debug)
        config_params = self.env['ir.config_parameter'].sudo()
        is_multilevel_menu = config_params.get_param('cdm_theme.is_multilevel_menu', False)
        icon_menu_sidebar_default = config_params.get_param('cdm_theme.icon_menu_sidebar_default', 'icon-mediclowd-logo')
        menu_root['is_multilevel_menu'] = is_multilevel_menu
        menu_root['icon_menu_sidebar_default'] = icon_menu_sidebar_default

        return menu_root
