from odoo import models, fields, _, api


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    is_multilevel_menu = fields.Boolean('Is Multilevel Menu',
                                        config_parameter='cdm_theme.is_multilevel_menu')

    is_button_reload = fields.Boolean('Button reload',
                                      config_parameter='cdm_theme.is_button_reload')

    icon_menu_sidebar_default = fields.Char('Default icon menu sidebar',
                                            config_parameter='cdm_theme.icon_menu_sidebar_default')
