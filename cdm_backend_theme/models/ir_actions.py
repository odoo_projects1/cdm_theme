# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import odoo
from odoo import api, fields, models, tools, SUPERUSER_ID, _


class IrActionsActWindow(models.Model):
    _inherit = 'ir.actions.act_window'

    def read(self, fields=None, load='_classic_read'):
        """ call the method get_empty_list_help of the model and set the window action help message
        """
        result = super(IrActionsActWindow, self).read(fields, load=load)
        for values in result:
            config_params = self.env['ir.config_parameter'].sudo()
            is_button_reload = config_params.get_param('cdm_theme.is_button_reload', False)
            values['actionReload'] = is_button_reload
        return result
