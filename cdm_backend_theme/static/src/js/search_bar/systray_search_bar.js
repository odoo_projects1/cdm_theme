odoo.define('cdm_theme.SearchBarMenu', function (require) {
    var SearchBar = require('web.SearchBar');
    var GroupFacet = require('cdm_theme.GroupSearchFacet');
    var SearchFacet = require('web.SearchFacet');

    var CdmThemeSearchBarMenu = SearchBar.extend({
        template: 'cdm_theme.SearchView.SearchBar',
        events: _.extend({}, SearchBar.prototype.events, {
            'click input': 'onInputFocus',
            'click .trigger_search': 'onInputFocus'
        }),

        init: function (parent, params) {
            this._super.apply(this, arguments);
            this.advancedSearchBoxOpened = parent.advancedSearchBoxOpened;
        },

        start: function () {
            this.$input = this.$('input');
            var self = this;
            var defs = [];

            var facets = [];
            if (this.facets.length < 3) {
                facets = this.facets;
            } else {
                var firstFacet = this.facets[0];
                var otherFacets = this.facets.slice(1);
                facets = [firstFacet, otherFacets];
            }

            _.each(facets, function (facet) {
                defs.push(self._renderFacet(facet));
            });

            defs.push(this._setupAutoCompletion());
            return Promise.all(defs);
        },

        _renderFacet: function (facet) {
            var searchFacet;
            if (Array.isArray(facet)) {
                searchFacet = new GroupFacet(this, facet);
            } else {
                searchFacet = new SearchFacet(this, facet);
            }

            this.searchFacets.push(searchFacet);
            return searchFacet.appendTo(this.$('.facets_container'));
        },

        onInputFocus: function () {
            this.$('input').focus();
            this.$('.facets_container').animate({
                width: 0
            }, 100);

            this.trigger_up('on_input_click');

        },
        onInputBlur: function () {
            var self = this;

            if (!this.$el) {
                return;
            }

            // clone the search bar to calculate real width of the facets container to animate to
            // since we don't know the real width of the facets container
            var $cloneElmn = $('.cdm_theme_search_bar').clone();
            $cloneElmn.appendTo('body');
            var width = $cloneElmn.find('.facets_container').css({width: 'auto'}).outerWidth();
            $cloneElmn.remove();

            this.$('.facets_container').animate({
                width: width
            }, 100, function () {
                self.$el.removeClass('advanced_search_opened');
            });
        },

        _onKeydown: function (e) {
            if (this._isInputComposing) {
                return;
            }

            if (e.which === $.ui.keyCode.BACKSPACE) {
                if (this.$input.val() === '') {
                    return;
                }
            }

            this._super.apply(this, arguments);
        }
    });

    return CdmThemeSearchBarMenu;
});