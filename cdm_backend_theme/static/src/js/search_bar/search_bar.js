odoo.define('cdm_theme.SearchBarMenu.Custom', function (require) {
    var SearchBar = require('web.SearchBar');

    SearchBar.include({
        start: function () {
            var self = this;
            return this._super.apply(this, arguments).then(function () {
                if (self.facets.length > 0) {
                    self.$el.addClass('has_facets');
                }
            });
        }
    });
});