odoo.define('cdm_theme.settings', function (require) {
    "use strict";

    var BaseSetting = require('base.settings');
    BaseSetting.Renderer.include({
        start: function() {
            var self = this;

            var $controller = this.getParent();
            return this._super.apply(this, arguments).then(function() {
                if ($controller.modelName === 'res.config.settings') {
                    var $oContent = self.$el.closest('.o_content');
                    $oContent.css('height', '100%');
                }
            });
        }
    })

});