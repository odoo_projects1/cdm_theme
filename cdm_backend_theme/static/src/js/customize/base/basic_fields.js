odoo.define('cdm_theme.basic_fields', function (require) {
    "use strict";

    var FieldChar = require('web.basic_fields').FieldChar;

    FieldChar.include({

        _renderReadonly: function () {
            if (this.attrs['class'] && this.attrs['class'].indexOf('show-original-tooltip') > -1) {
                this.$el.tooltip('disable');
                this.$el.attr('title', this.value);
            }

            this._super.apply(this, arguments);
        },
    });
});
1
