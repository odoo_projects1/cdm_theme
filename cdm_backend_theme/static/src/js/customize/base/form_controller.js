odoo.define('cdm_theme.FormController', function (require) {
    "use strict";
    var FormController = require('web.FormController');
    var core = require('web.core');
    var dialogs = require('web.view_dialogs');

    var _t = core._t;

    FormController.include({
        init: function (parent, model, renderer, params) {
            this._super.apply(this, arguments);

            // count number of time save action is called for this form
            // -> reload the source list controller if there's any saving is called
            this.saveCounter = 0;
        },

        renderButtons: function ($node) {
            this._super.apply(this, arguments);
            this._bindCustomButtons();
        },


        _bindCustomButtons: function () {
            if (!this.$buttons) {
                return;
            }

            var self = this;
            this.$buttons.on('click', '.o_button_reload_page', function () {
                self.update({});
            });
        },

        saveRecord: function () {
            var self = this;

            return this._super.apply(this, arguments).then(function (fieldNames) {
                if (fieldNames && fieldNames.length > 0) {
                    self.saveCounter += 1;
                }

                return fieldNames;
            });
        },

        _onDiscard: function () {
            if (this.renderer.state.context.close_after_discard) {
                core.bus.trigger('close_dialogs');
            } else {
                this._discardChanges();
            }
        },

        changeZIndexBackdropModal: function () {
            var zIndex = 1040;
            $('.modal-backdrop').each(function () {
                $(this).css({'z-index': zIndex})
                zIndex = 1050;
            })
        },
        _onOpenRecord: function (ev) {
            var target = (ev.target) || {};
            var arch = (target && target.arch) || {};
            var attrs = (arch && arch.attrs) || {};
            var fullscreen = false;
            if (attrs.hasOwnProperty('fullscreen')) {
                fullscreen = attrs.fullscreen === '1';
            }

            if (!fullscreen) {
                this._super.apply(this, arguments);
                return;
            }
            ev.stopPropagation();
            var self = this;
            var record = this.model.get(ev.data.id, {raw: true});
            var modal = new dialogs.FormViewDialog(self, {
                context: ev.data.context,
                fields_view: ev.data.fields_view,
                on_saved: ev.data.on_saved,
                on_remove: ev.data.on_remove,
                readonly: ev.data.readonly,
                deletable: ev.data.deletable,
                res_id: record.res_id,
                res_model: record.model,
                title: _t("Open: ") + ev.data.string,
            });
            modal.fullscreen = fullscreen;
            modal.open();
        },

        _onOpenOne2ManyRecord: async function (ev) {
            var target = (ev.target) || {};
            var attrs = (target && target.attrs) || {};

            if (attrs.options.no_open) {
                return;
            }

            var fullscreen = attrs.fullscreen === '1';

            var openViewEditToggleMode = attrs.edit === '1' && this.mode === 'readonly';
            if (openViewEditToggleMode) {
                this._onOpenAndEditOne2ManyRecord(ev, fullscreen);
            } else {
                ev.data.fullscreen = fullscreen;
                this._doOpenOne2ManyRecord.apply(this, arguments);
            }
        },

        _doOpenOne2ManyRecord: async function (ev) {
            ev.stopPropagation();
            var data = ev.data;
            var record;
            if (data.id) {
                record = this.model.get(data.id, {raw: true});
            }

            // Sync with the mutex to wait for potential onchanges
            await this.model.mutex.getUnlockedDef();

            var modal = new dialogs.FormViewDialog(this, {
                context: data.context,
                domain: data.domain,
                fields_view: data.fields_view,
                model: this.model,
                on_saved: data.on_saved,
                on_remove: data.on_remove,
                parentID: data.parentID,
                readonly: data.readonly,
                deletable: record ? data.deletable : false,
                recordID: record && record.id,
                res_id: record && record.res_id,
                res_model: data.field.relation,
                shouldSaveLocally: true,
                title: (record ? _t("Open: ") : _t("Create ")) + (ev.target.string || data.field.string),
            });

            modal.fullscreen = data.fullscreen;
            this.currentDialog = modal;
            modal.open();
        },

        _openEditModal: function (ev, parentModal, fullscreen) {
            var data = ev.data;
            var record = data.id && this.model.get(data.id, {raw: true});
            var self = this;

            var childModal = new dialogs.FormViewDialog(self, {
                context: data.context,
                domain: data.domain,
                model: self.model,
                parent: parentModal,
                readonly: false,
                deletable: record ? data.deletable : false,
                res_id: record && record.res_id,
                res_model: data.field.relation,
                buttons: [
                    {
                        text: _t("Save"),
                        classes: "btn-primary",
                        click: function () {
                            childModal._save().then(function () {
                                parentModal.closeOnClosingChildModal = false;
                                if (parentModal.hasOwnProperty('currentParent')){
                                    parentModal.currentParent.update({});
                                }
                                parentModal.form_view.update({}).then(function () {
                                    childModal.destroy();
                                });
                            });
                            if (self.getParent() && self.getParent().getParent()) {
                                var parent = self.getParent().getParent();
                                if (parent.hasOwnProperty('conversation') && parent.getParent()) {
                                    parent.getParent().reloadCurrentThread();
                                }
                            }
                        }
                    },
                    {
                        text: _t("Discard"),
                        classes: "btn-secondary o_form_button_cancel",
                        click: function () {
                            parentModal.closeOnClosingChildModal = false;
                            childModal.destroy();
                        },
                    }
                ],
                title: (record ? _t("Edit: ") : _t("Create ")) + (ev.target.string || data.field.string),
            });

            childModal.fullscreen = fullscreen;
            childModal.open().opened().then(function () {
                self.changeZIndexBackdropModal()
                parentModal.closeOnClosingChildModal = true;
                childModal.$modal.on('hidden.bs.modal', function (e) {
                    if (parentModal.closeOnClosingChildModal) {
                        parentModal.close();
                    }
                })
            })
        },

        _onOpenAndEditOne2ManyRecord: function (ev, fullscreen) {
            ev.stopPropagation();

            var data = ev.data;
            var record = data.id && this.model.get(data.id, {raw: true});
            var self = this;
            data.context['isPopup'] = true;
            var classBtn = '';
            if (record.data.hasOwnProperty('is_editable') && !record.data.is_editable) {
                classBtn = 'o_hidden';
            }

            var parentModal = new dialogs.FormViewDialog(this, {
                context: data.context,
                domain: data.domain,
                fields_view: data.fields_view,
                model: this.model,
                on_saved: data.on_saved,
                on_remove: data.on_remove,
                parentID: data.parentID,
                readonly: data.readonly,
                deletable: record ? data.deletable : false,
                recordID: record && record.id,
                res_id: record && record.res_id,
                res_model: data.field.relation,
                shouldSaveLocally: true,
                buttons: [
                    {
                        text: _t("Edit"),
                        classes: "btn-primary " + classBtn,
                        click: function () {
                            self._openEditModal(ev, parentModal, fullscreen);
                        }
                    },
                    {
                        text: _t("Discard"),
                        classes: "btn-secondary o_form_button_cancel " + classBtn,
                        close: true,
                        click: function () {
                            parentModal.form_view.model.discardChanges(parentModal.form_view.handle, {
                                rollback: parentModal.shouldSaveLocally,
                            });
                        },
                    }
                ],
                title: (record ? _t("Open: ") : _t("Create ")) + (ev.target.string || data.field.string),
            });

            parentModal.fullscreen = fullscreen;
            parentModal.closeOnClosingChildModal = true;
            parentModal.currentParent = this;
            parentModal.open().opened().then(function () {
                self.currentDialog = parentModal;
                self.changeZIndexBackdropModal();
            });
        },

        _getFieldWidget: function (fieldName) {
            var allHandleWidget = this.renderer.allFieldWidgets[this.handle]
            if (typeof allHandleWidget === 'object') {
                for (var i in allHandleWidget) {
                    if (allHandleWidget[i].name === fieldName) {
                        return allHandleWidget[i]
                    }
                }
            }
            return false
        },
        getControllerList: function () {
            var controller = null;
            if (this.getParent()) {
                var parent = this.getParent();
                for (var child in parent.controllers) {
                    if (parent.controllers[child].hasOwnProperty('viewType') && parent.controllers[child].viewType === 'list') {
                        controller = parent.controllers[child].hasOwnProperty('widget') ? parent.controllers[child].widget : null;
                        break;
                    }
                }
            }
            return controller;
        },
        actionModel: function (event) {
            var self = this;
            var attrs = event.data.attrs;
            var record = event.data.record;
            var data = record.data;
            var listController = this.getControllerList();
            this._rpc({
                model: record.model,
                method: attrs.name,
                args: [
                    data.id
                ]
            }).then(function (result) {
                self.update({}).then(function () {
                    if (listController) {
                        listController.update({});
                    }
                });
            });
        },

        _onButtonClicked: function (ev) {
            var attrs = ev.data.attrs;
            ev.stopPropagation();
            if (attrs && attrs.name === 'trigger_create_edit_popup') {
                var target_field = attrs.target;
                var fieldWidget = this._getFieldWidget(target_field);
                if (fieldWidget && fieldWidget.formatType === 'many2one') {
                    var context = fieldWidget._createContext(_('Delivery Address'));
                    fieldWidget._searchCreatePopup("form", false, context)
                }
                return
            }
            if (attrs && attrs.hasOwnProperty('data-type') && attrs['data-type'] === 'action-model') {
                this.actionModel(ev);
                return;
            }
            return this._super.apply(this, arguments);
        },
    })
});
