odoo.define('cdm_theme.Dialog', function (require) {
    "use strict";

    var Dialog = require('web.Dialog');

    Dialog.include({
        willStart: function () {
            var self = this;
            return this._super.apply(this, arguments).then(function () {
                if (self.renderFooter && self.$footer.length === 0) {
                    self.$footer = self.$modal.find(".action-buttons");
                    self.set_buttons(self.buttons);
                }
            });
        }
    });

});
