odoo.define('cdm_theme.form_renderer', function (require) {
    "use strict";

    var FormRenderer = require('web.FormRenderer');
    //Required to overwrite
    require('mail.form_renderer');

    /**
     * Include the FormRenderer to instanciate the chatter area containing (a
     * subset of) the mail widgets (mail_thread, mail_followers and mail_activity).
     */
    FormRenderer.include({
        _renderView: function () {
            var self = this;
            return this._super.apply(this, arguments).then(function () {
                self.$('.action-bar [data-toggle="tooltip"]').tooltip();
            });
        },
    });
});
