odoo.define('cdm_theme.composer.Chatter', function (require) {
    "use strict";
    var ChatterComposer = require('mail.composer.Chatter');
    ChatterComposer.include({
        events: _.extend({}, ChatterComposer.prototype.events, {
            'click .o_composer_button_discard': '_onClickDiscard',
        }),

        start: function () {
            var self = this;
            this.$el.addClass('oe_read_only');
            return this._super.apply(this, arguments).then(function () {
                self.showDiscardButton();
            });
        },

        showDiscardButton: function () {
            var $discard = this.$('.o_composer_button_discard');
            $discard.removeClass('d-none');
            $discard.addClass('d-md-inline-block');
        },

        _onClickDiscard: function (e) {
            e.stopPropagation();
            this.trigger('close_composer');
        }
    });

});
