odoo.define('cdm_theme.ActionManager', function (require) {
    var ActionManager = require('web.ActionManager');
    var dom = require('web.dom');
    var core = require('web.core');
    var bus = core.bus;

    ActionManager.include({
        className: '',
        template: 'ActionManager',
        _appendController: function (controller) {
            // Replace parent action with new to clone search and change main class
            dom.append(this.$el.children('.o_action_manager'), controller.widget.$el, {
                in_DOM: this.isInDOM,
                callbacks: [{widget: controller.widget}],
            });

            if (controller.scrollPosition) {
                this.trigger_up('scrollTo', controller.scrollPosition);
            }

            /*--------------------------------------
               Render header search bar after each time rendering the controller
            ---------------------------------------*/
            this.attachHeaderSearchBox(controller);

            bus.trigger('on_push_controller', controller);
        },

        attachHeaderSearchBox: function (controller) {
            var $headerSearchBar = $('nav.o_main_navbar .cdm_theme_search_bar');
            var $extraSearchBar = $('nav.o_main_navbar .extra_search_bar');

            $headerSearchBar.empty();
            $extraSearchBar.empty();

            $('nav.o_main_navbar .advanced_filter').empty();

            var controlPanel = controller.widget._controlPanel;
            if (controlPanel && controlPanel.renderer) {

                var defs = [];
                var headerSearchBar = controlPanel.renderer.headerSearchBar;

                if (headerSearchBar) {
                    controlPanel.renderer.advancedSearchBoxOpened = false;
                    headerSearchBar.advancedSearchBoxOpened = false;

                    defs.push(headerSearchBar.appendTo($headerSearchBar))
                }

                var extraSearchBar = controlPanel.renderer.extraSearchBar;
                if (extraSearchBar) {
                    defs.push(extraSearchBar.appendTo($extraSearchBar))
                }

                if (defs.length > 0) {
                    Promise.all(defs).then(function () {
                        controlPanel.renderer.headerMenusSetup = false;
                        controlPanel.renderer._doSetupOrUpdateHeaderMenus();
                    })
                }
            }
        }
    });
});