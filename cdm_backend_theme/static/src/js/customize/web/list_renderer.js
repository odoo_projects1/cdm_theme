odoo.define('cdm_theme.ListRenderer', function (require) {
    "use strict";

    var ListRenderer = require('web.ListRenderer');
    var Pager = require('web.Pager');
    var core = require('web.core');
    var bus = core.bus;
    var _t = core._t;

    ListRenderer.include({
        showGroupByMenu: false,
        toShowGroup: null,

        _renderView: function () {
            if (this.showGroupByMenu) {
                var self = this;
                var listController = this.getParent();
                return this._super.apply(this, arguments).then(function () {
                    self._renderGroupBySidebar();

                    if (!self._isShowGroupBySidebar()) {
                        self.toShowGroup = null;
                        self._updateControlPanelTitle(listController._originalTitle);
                    }
                });
            } else {
                return this._super.apply(this, arguments);
            }
        },

        mapDataIconGroupBy: function (iconDefault) {
            var self = this;
            var promise = new Promise(function (resolve, reject) {
                var state = self.state;
                var data = state.data;
                if (state.groupedBy.length === 1) {
                    var fieldGroupBy = state.groupedBy[0];
                    var fieldInfo = state.fields[fieldGroupBy];
                    var listModel = ['cdm_sale.order_approval_step', 'crm.stage'];
                    if (fieldInfo && fieldInfo.type === 'many2one' && listModel.includes(fieldInfo.relation)) {
                        self._rpc({
                            model: fieldInfo.relation,
                            method: 'search_read',
                            fields: ['name', 'icon']
                        }).then(function (result) {
                            data.forEach(function (item) {
                                var val = _.find(result, function (value) {
                                    return value.id === item.res_id;
                                })
                                if (val && val.icon) {
                                    item.icon = val.icon;
                                }
                            })
                            resolve();
                        })
                    } else if (fieldGroupBy === 'status' && state.model === 'cdm_crm.task') {
                        self._rpc({
                            model: 'ir.model.fields',
                            method: 'search_read',
                            fields: ['id'],
                            domain: [['name', '=', 'status'], ['model', '=', 'cdm_crm.task']]
                        }).then(function (result) {
                            if (result.length > 0) {
                                self._rpc({
                                    model: 'ir.model.fields.selection',
                                    method: 'search_read',
                                    fields: ['name', 'icon', 'value'],
                                    domain: [['field_id', '=', result[0]['id']]]
                                }).then(function (result) {
                                    data.forEach(function (item) {
                                        var val = _.find(result, function (value) {
                                            var check = false;
                                            item.domain.forEach(function (domain) {
                                                if (domain.length === 3 && domain[0] === fieldGroupBy && domain[2] === value.value) {
                                                    check = true;
                                                }
                                            })
                                            return check;
                                        })
                                        if (val && val.icon) {
                                            item.icon = val.icon;
                                        }
                                    })
                                    resolve();
                                })
                            }
                        })
                    } else {
                        resolve();
                    }
                } else {
                    resolve();
                }

            });

            return promise;
        },

        _renderGroupBySidebar: function () {
            var self = this;
            this.mapDataIconGroupBy().then(function () {
                bus.trigger('on_render_list_view', self);
            })
        },

        showGroup: function (group) {
            if (!group) {
                return;
            }

            if (this.toShowGroup === group) {
                return;
            }

            this._collapseCurrentShowingGroup();
            this.toShowGroup = this.getParent().model.localData[group.id];

            var self = this;
            this._renderView().then(function () {
                if (!self.toShowGroup || self.toShowGroup.isOpen) {
                    return;
                }

                self.trigger_up('toggle_group', {
                    group: self.toShowGroup,
                    onSuccess: function () {
                        self._updateSelection();
                    }
                });
            });
        },

        _collapseCurrentShowingGroup: function () {
            if (!this.toShowGroup) {
                return;
            }

            var newGroup = this.getParent().model.localData[this.toShowGroup.id];

            if (newGroup.isOpen) {
                this.trigger_up('toggle_group', {
                    group: newGroup
                });
            }
        },

        _updateControlPanelTitle: function (title) {
            if (!title) {
                title = _t('Unclassified');
            }

            title = String(title);

            var listController = this.getParent();
            listController._title = title;

            var cpContent = _.extend({}, listController.controlPanelElements);

            var def = Promise.resolve(true);
            if (this.toShowGroup) {
                cpContent.$pager = $('<div/>');
                def = this._renderCustomGroupPager(this.toShowGroup, cpContent.$pager);
            } else {
                cpContent.$pager = listController._originalPager;
            }

            def.then(function () {
                listController.controlPanelElements = cpContent;
            })
        },

        _renderCustomGroupPager: function (group, $node) {
            var pager = new Pager(this, group.count, group.offset + 1, group.limit);
            pager.on('pager_changed', this, function (newState) {
                var self = this;
                pager.disable();
                this.trigger_up('load', {
                    id: group.id,
                    limit: newState.limit,
                    offset: newState.current_min - 1,
                    on_success: function (reloadedGroup) {
                        var existGroup = self.state.data.find(function (_g) {
                            return group.id === _g.id;
                        });

                        _.extend(existGroup, reloadedGroup);

                        self._renderView().then(function () {
                            pager.enable();
                        });
                    },
                    on_fail: pager.enable.bind(pager)
                });
            });
            // register the pager so that it can be destroyed on next rendering
            // this.pagers.push(pager);
            this.groupPager = pager;

            return this.groupPager.appendTo($node);
        },

        _isShowGroupBySidebar: function () {
            return this.isGrouped && this.showGroupByMenu
                && this.state.groupedBy && this.state.groupedBy.length === 1
                && this.state.data && this.state.data.length > 0;
        },

        _computeAggregates: function () {
            var computeByGroup = this._isShowGroupBySidebar() && !this.selection.length;
            if (computeByGroup) {
                var data = this.state.data.filter(function (group) {
                    return group.isOpen;
                });

                _.each(this.columns, this._computeColumnAggregates.bind(this, data));
            } else {
                this._super.apply(this, arguments);
            }
        },

        _renderGroups: function (data, groupLevel) {
            if (!this._isShowGroupBySidebar()) {
                return this._super.apply(this, arguments);
            }

            if (!this.toShowGroup) {
                return;
            }

            this._updateControlPanelTitle(this.toShowGroup.value);

            var result = [];
            var self = this;
            groupLevel = groupLevel || 0;
            var $tbody = $('<tbody>');
            _.each(data, function (group) {
                if (self.toShowGroup.res_id !== group.res_id) {
                    return;
                }

                if (!$tbody) {
                    $tbody = $('<tbody>');
                }

                var $groupRow = self._renderGroupRow(group, groupLevel);
                $groupRow.addClass('o_hidden');
                $tbody.append($groupRow);

                if (group.data.length) {
                    result.push($tbody);
                    result = result.concat(self._renderGroup(group, groupLevel));
                    $tbody = null;
                }
            });

            if ($tbody) {
                result.push($tbody);
            }

            return result;
        },

        _renderBodyCell: function (record, node, colIndex, options) {
            if (typeof record.evalModifiers == "function") {
                var result = this._super.apply(this, arguments);
                var name = node.attrs.name;
                var field = this.state.fields[name];
                if (field && field.type === 'html') {
                    result.attr('title', '')
                }
                return result
            }
        }
    });
});
