odoo.define('cdm_theme.ControlPanelController', function (require) {
    var ControlPanelController = require('web.ControlPanelController');

    ControlPanelController.include({
        _onFacetRemoved: function (ev) {
            ev.stopPropagation();
            var group = ev.data.group;

            // if we remove a GroupSearchFacet -> the group here will be an array of facets
            // therefore we need to specially handle this case
            // ref: GroupSearchFacet._onFacetRemove
            if (Array.isArray(group)) {
                var self = this;
                _.each(group, function (one) {
                    self.model.deactivateGroup(one.id);
                });

                this._reportNewQueryAndRender();
            } else {
                this._super.apply(this, arguments);
            }
        }
    });
});