odoo.define('cdm.ReportActionManager', function (require) {
    "use strict";

    //Require before module
    require('web.ReportActionManager');

    var core = require('web.core');
    var _t = core._t;
    var Dialog = require('web.Dialog');
    var ActionManager = require('web.ActionManager');

    ActionManager.include({
        _executeReportAction: function (action, options) {
            var self = this;

            if (action.report_type === 'qweb-html') {
                return this._executeReportClientAction(action, options);
            } else if (action.report_type === 'qweb-pdf') {
                action.target = 'new';
                return self._executeReportPDFAction(action, options);
            } else if (action.report_type === 'qweb-text') {
                return self._triggerDownload(action, options, 'text');
            } else {
                console.error("The ActionManager can't handle reports of type " +
                    action.report_type, action);
                return Promise.reject();
            }
        },
        _executeReportPDFAction: function (action, options) {
            //Todo: PDF format to bad, restyle and change link to pdf later
            var urls = this._makeReportUrls(action);
            var dialog = new Dialog(this, {
                size: 'large',
                title: action.name,
                $content: '<iframe style="height: 100vh" frameborder="0" src="' + urls.html + '"/>',
                buttons: [
                    {
                        text: _t('Print'), classes: 'btn-primary', close: false, click: function (event) {
                            var iframe = dialog.$el[0].contentWindow;
                            iframe.print();
                        }
                    },
                    {text: _t('Cancel'), close: true}
                ]
            }).open({shouldFocusButtons: true});
            return Promise.reject();
        },
    });
});