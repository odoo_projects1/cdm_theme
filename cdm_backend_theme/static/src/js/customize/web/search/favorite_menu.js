odoo.define('cdm_theme.FavoriteMenu', function (require) {
    "use strict";
    var FavoriteMenu = require('web.FavoriteMenu');
    var CdmFavoriteMenu = FavoriteMenu.extend({
        template: 'cdm_theme.DropdownMenu',
        events: _.extend({}, FavoriteMenu.prototype.events, {
            'click button.btn': '_onActiveMenu'
        }),
        init: function () {
            this._super.apply(this, arguments);
            this.dropdownStyle.mainButton.class = 'btn btn-secondary o_favorites_menu_button' + (this.dropdownSymbol ? 'o-no-caret' : '');
        },

        _onActiveMenu: function () {
            this.trigger_up('on_switching_advanced_search_menu', {type: 'favorite'});
            this.$('> button.btn').addClass('active');
            this.$('.o_dropdown_menu').addClass('show');
        }
    });

    return CdmFavoriteMenu;
});