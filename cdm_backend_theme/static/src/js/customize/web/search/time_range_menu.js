odoo.define('cdm_theme.TimeRangeMenu', function (require) {
    "use strict";
    var TimeRangeMenu = require('web.TimeRangeMenu');
    var CdmTimeRangeMenu = TimeRangeMenu.extend({
        events: _.extend({}, TimeRangeMenu.prototype.events, {
            'click button.btn': '_onActiveMenu'
        }),
        template: 'cdm_theme.TimeRangeMenu',

        _onActiveMenu: function () {
            this.trigger_up('on_switching_advanced_search_menu', {type: 'timeRange'});
            this.$('> button.btn').addClass('active');
            this.$('.o_dropdown_menu').addClass('show');
        }
    });

    return CdmTimeRangeMenu;
});