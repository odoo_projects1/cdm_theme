odoo.define('cdm_theme.GroupByMenu', function (require) {
    "use strict";
    var GroupByMenu = require('web.GroupByMenu');
    var CdmGroupByMenu = GroupByMenu.extend({
        events: _.extend({}, GroupByMenu.prototype.events, {
            'click button.btn': '_onActiveMenu'
        }),
        template: 'cdm_theme.DropdownMenu',
        init: function () {
            this._super.apply(this, arguments);
            this.dropdownStyle.mainButton.class = 'btn btn-secondary' + (this.dropdownSymbol ? 'o-no-caret' : '');
        },

        _onActiveMenu: function () {
            this.trigger_up('on_switching_advanced_search_menu', {type: 'groupby'});
            this.$('> button.btn').addClass('active');
            this.$('.o_dropdown_menu').addClass('show');
        }
    });

    return CdmGroupByMenu;
});
