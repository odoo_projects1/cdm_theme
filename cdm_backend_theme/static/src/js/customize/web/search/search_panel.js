odoo.define('cdm_theme.SearchPanel', function (require) {
    "use strict";

    /**
     * This file defines the SearchPanel widget for Kanban. It allows to
     * filter/manage data easily.
     */

    var core = require('web.core');
    var Domain = require('web.Domain');
    var pyUtils = require('web.py_utils');
    var viewUtils = require('web.viewUtils');
    var SearchPanel = require('web.SearchPanel');

    var qweb = core.qweb;
    var _t = core._t;

// defaultViewTypes is the list of view types for which the searchpanel is
// present by default (if not explicitly stated in the 'view_types' attribute
// in the arch)
    var defaultViewTypes = ['kanban', 'tree'];

    /**
     * Given a <searchpanel> arch node, iterate over its children to generate the
     * description of each section (being either a category or a filter).
     *
     * @param {Object} node a <searchpanel> arch node
     * @param {Object} fields the fields of the model
     * @returns {Object}
     */
    function _processSearchPanelNode(node, fields) {
        var sections = {};
        node.children.forEach(function (childNode, index) {
            if (childNode.tag !== 'field') {
                return;
            }
            if (childNode.attrs.invisible === "1") {
                return;
            }
            var fieldName = childNode.attrs.name;
            var type = childNode.attrs.select === 'multi' ? 'filter' : 'category';

            var sectionId = _.uniqueId('section_');
            var section = {
                color: childNode.attrs.color,
                description: childNode.attrs.string || fields[fieldName].string,
                fieldName: fieldName,
                icon: childNode.attrs.icon,
                id: sectionId,
                index: index,
                type: type
            };
            if (section.type === 'category') {
                section.icon = section.icon || 'icon-filter';
            } else if (section.type === 'filter') {
                var disableCounterValue = childNode.attrs.disable_counters;
                if (!disableCounterValue) {
                    try {
                        disableCounterValue = JSON.parse(childNode.attrs.modifiers).disable_counters;
                        disableCounterValue = disableCounterValue.toString()
                    } catch (e) {
                    }
                }
                section.disableCounters = !!pyUtils.py_eval(disableCounterValue || '0');
                section.domain = childNode.attrs.domain || '[]';
                section.groupBy = childNode.attrs.groupby;
                section.icon = section.icon || 'icon-tag';
            }
            sections[sectionId] = section;
        });
        return sections;
    }

    SearchPanel.include({
        collapsed: false,
        events: _.extend({}, SearchPanel.prototype.events, {
            'click .toggle-sidebar': '_toggleCollapseSearchPanel',
            'click a.o_search_panel_section_header': '_onClickHeader'
        }),
        computeSearchPanelParams: function (viewInfo, viewType) {
            var searchPanelSections;
            var classes;
            if (viewInfo) {
                var arch = viewUtils.parseArch(viewInfo.arch);
                viewType = viewType === 'list' ? 'tree' : viewType;
                arch.children.forEach(function (node) {
                    if (node.tag === 'searchpanel') {
                        var attrs = node.attrs;
                        var viewTypes = defaultViewTypes;
                        if (attrs.view_types) {
                            viewTypes = attrs.view_types.split(',');
                        }
                        if (attrs.class) {
                            classes = attrs.class.split(' ');
                        }
                        if (viewTypes.indexOf(viewType) !== -1) {
                            searchPanelSections = _processSearchPanelNode(node, viewInfo.fields);
                        }
                    }
                });
            }
            return {
                sections: searchPanelSections,
                classes: classes
            };
        },
        init: function (parent, params) {
            this._super.apply(this, arguments);
            this.collapsed = this.call('local_storage', 'getItem', 'searchpanel_collapsed');
        },
        _getCategoryDefaultValue: function (category, validValues) {
            // set active value from context
            var value = this.defaultValues[category.fieldName];
            // if not set in context, or set to an unknown value, set active value
            // from localStorage
            // if (!_.contains(validValues, value)) {
            //     var storageKey = this._getLocalStorageKey(category);
            //     return this.call('local_storage', 'getItem', storageKey);
            // }
            return value;
        },
        _renderCollapseButton: function () {
            return '<a class="toggle-sidebar">\n' +
                '                <span class="icon icon icon-page_first"></span>\n' +
                '                <span class="text">' + _t('Collapse') + '</span>\n' +
                '            </a>';
        },
        _toggleCollapseSearchPanel: function () {
            if (this.collapsed) {
                this._openSearchPanel();
            } else {
                this._collapseSearchPanel();
            }
        },
        _collapseSearchPanel: function () {
            this.$el.addClass('searchpanel_collapsed');
            this.collapsed = true;
            this.call('local_storage', 'setItem', 'searchpanel_collapsed', this.collapsed);
        },
        _openSearchPanel: function () {
            this.$el.removeClass('searchpanel_collapsed');
            this.collapsed = false;
            this.call('local_storage', 'setItem', 'searchpanel_collapsed', this.collapsed);
        },
        _render: function () {
            var self = this;
            this.$el.empty();
            var $filterContainer = $('<div class="search_panel_inner"></div>');

            // sort categories and filters according to their index
            var categories = Object.keys(this.categories).map(function (categoryId) {
                return self.categories[categoryId];
            });
            var filters = Object.keys(this.filters).map(function (filterId) {
                return self.filters[filterId];
            });
            var sections = categories.concat(filters).sort(function (s1, s2) {
                return s1.index - s2.index;
            });

            sections.forEach(function (section) {
                section.allTitle = _t('All options');
                var collapseKey = self.model + '_collapse_' + section.fieldName;
                var isCollapsed = self.call('local_storage', 'getItem', collapseKey, false);
                section.collapse_class = isCollapsed ? 'collapsed' : '';
                if (Object.keys(section.values).length) {
                    if (section.type === 'category') {
                        $filterContainer.append(self._renderCategory(section));
                    } else {
                        $filterContainer.append(self._renderFilter(section));
                    }
                }
            });
            $filterContainer.find('.o_search_panel_section.collapsed > .o_search_panel_field').hide(0);

            this.$el.append($filterContainer);
            this.$el.append(this._renderCollapseButton());

            if (this.collapsed) {
                this._collapseSearchPanel();
            } else {
                this._openSearchPanel();
            }
        },
        _onClickHeader: function (e) {
            if (this.collapsed) {
                e.preventDefault();
                return;
            }
            var $currentTarget = $(e.currentTarget);
            var $parentTarget = $currentTarget.parent();
            var sectionName = $parentTarget.attr('data-field-name');
            var collapseKey = this.model + '_collapse_' + sectionName;
            if (sectionName) {
                if ($parentTarget.hasClass('collapsed')) {
                    $parentTarget.removeClass('collapsed');
                    $parentTarget.find('.o_search_panel_field').show(300);
                    this.call('local_storage', 'setItem', collapseKey, false);
                } else {
                    $parentTarget.addClass('collapsed');
                    $parentTarget.find('.o_search_panel_field').hide(300);
                    this.call('local_storage', 'setItem', collapseKey, true);
                }
            }
            e.preventDefault()
        }
    });

});
