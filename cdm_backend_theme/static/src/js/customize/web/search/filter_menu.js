odoo.define('cdm_theme.FilterMenu', function (require) {
    var FilterMenu = require('web.FilterMenu');
    var CdmFilterMenu = FilterMenu.extend({
        template: 'cdm_theme.DropdownMenu',
        events: _.extend({}, FilterMenu.prototype.events, {
            'click button.btn': '_onActiveMenu'
        }),
        init: function () {
            this._super.apply(this, arguments);
            this.dropdownStyle = {
                el: {class: 'btn-group o_dropdown', attrs: {}},
                mainButton: {
                    class: 'btn btn-secondary ' +
                        (this.dropdownSymbol ? 'o-no-caret' : '')
                }
            };
        },

        start: function () {
            var self = this;
            return this._super.apply(this, arguments).then(function () {
                self._onActiveMenu(); // active filter menu by default
            });
        },

        _onActiveMenu: function () {
            this.trigger_up('on_switching_advanced_search_menu', {type: 'filter'});
            this.$('> button.btn').addClass('active');
            this.$('.o_dropdown_menu').addClass('show');
        }
    });

    return CdmFilterMenu;
});