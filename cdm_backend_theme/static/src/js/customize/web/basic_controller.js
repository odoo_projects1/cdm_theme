odoo.define('cdm_theme.BasicController', function (require) {
    "use strict";

    var BasicController = require('web.BasicController');
    BasicController.include({
        init: function (parent, model, renderer, params) {
            this._super.apply(this, arguments);

        },
        start: function () {
            var parent = this.parent;
            if (parent && parent.hasOwnProperty('actions') && parent.actions) {
                var actions = parent.actions;
                var actionReload = false;
                for (var key in actions) {
                    var action = actions[key];
                    if (action && action.hasOwnProperty('actionReload')) {
                        actionReload = action.actionReload === "True";
                    }
                }
                this.activeActions['reload'] = actionReload;
            }
            return this._super.apply(this, arguments);
        },
        _notifyInvalidFields: function (invalidFields) {
            // we don't show invalid field warning anymore due to #1420
        }
    })
});
