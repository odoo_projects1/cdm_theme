odoo.define('cdm_theme.BasicModel', function (require) {
    "use strict";
    var BasicView = require('web.BasicView');

    BasicView.include({
        _processField: function (viewType, field, attrs) {
            var modifiers = attrs.modifiers;
            if (modifiers) {
                for (var attr in modifiers) {
                    if (!modifiers.hasOwnProperty(attr)) {
                        continue;
                    }

                    if (attr.startsWith('decoration-')) {
                        attrs[attr] = modifiers[attr];
                    }
                }
            }

            // field.onChange = "1";

            return this._super.apply(this, arguments);
        }
    });
});