odoo.define('cdm_theme.SidebarMenu', function (require) {
    "use strict";
    var core = require('web.core');
    var bus = core.bus;

    var Widget = require('web.Widget');
    var ToggleSidebarMixins = require('cdm_theme.ToggleSidebarMixins');

    var GroupBySidebar = Widget.extend({
        template: 'GroupBySidebar',
        events: {
            'click .o_menu_entry_lvl_2': 'onItemClicked'
        },
        init: function (parent, listRenderer) {
            this._super.apply(this, arguments);
            this.listRenderer = listRenderer;
            this.groups = listRenderer.state.data;
            this.iconDefault = parent.iconDefault;
        },
        onItemClicked: function (e) {
            var $target = $(e.currentTarget);
            var groupId = $target.data('group-id');
            var targetGroup = this.groups.find(function (_group) {
                return _group.id === groupId;
            });

            this.doShowGroup(targetGroup);
        },
        doShowGroup: function (group) {
            this.listRenderer.showGroup(group);
        },
        selectFirstItem: function () {
            if (!this.hasGroupToShow()) {
                this.doShowGroup(this.groups[0]);
            }
        },
        hasGroupToShow: function () {
            var toShowGroup = this.listRenderer.toShowGroup;
            var hasGroupToShow = toShowGroup && this.groups.some(function (group) {
                return _.isEqual(toShowGroup.domain, group.domain);
            });
            return hasGroupToShow;
        },
        isActiveGroup: function (group) {
            var toShowGroup = this.listRenderer.toShowGroup;
            return toShowGroup && toShowGroup.id === group.id;
        }
    });

    var SidebarMenu = Widget.extend(ToggleSidebarMixins, {
        template: 'SidebarMenu',
        events: _.extend({}, ToggleSidebarMixins.events, {}),
        open: false,
        collapsed: false,
        $currentSidebar: false,
        currentSidebar: false,
        currentMenu: false,
        sidebarMap: {},
        sidebars: [],
        iconDefault: '',

        init: function (parent, sidebars) {
            this._super.apply(this, arguments);
            this.sidebars = sidebars;
            for (var i = 0; i < sidebars.length; i++) {
                var xmlid = sidebars[i].xmlid;
                this.sidebarMap[xmlid] = sidebars[i]
            }
            $(window).bind('hashchange', this.on_hashchange);

            ToggleSidebarMixins.init.apply(this);
            if (parent.hasOwnProperty('menu_data') && parent.menu_data.hasOwnProperty('icon_menu_sidebar_default')) {
                this.iconDefault = parent.menu_data.icon_menu_sidebar_default;
            }

            bus.on('on_render_list_view', this, this.doRenderGroupBySidebar);
            bus.on('on_push_controller', this, this.doAfterRenderView)
        },

        doAfterRenderView: function (controller) {
            if (this.groupBySidebar) {
                this.groupBySidebar.destroy();
                this.groupBySidebar = null;
            }

            var listRenderer = controller.widget.renderer;
            if (!listRenderer || !listRenderer.showGroupByMenu) {
                return;
            }
            var self = this;
            if (listRenderer && listRenderer.viewType === 'list') {
                listRenderer.mapDataIconGroupBy().then(function () {
                    self.doRenderGroupBySidebar(listRenderer);
                })
            } else {
                self.doRenderGroupBySidebar(listRenderer);
            }
        },

        doRenderGroupBySidebar: function (listRenderer) {
            if (!listRenderer || !listRenderer.showGroupByMenu) {
                return;
            }

            var controller = listRenderer.getParent();
            var isNestedModal = controller.getParent() && controller.getParent().backdrop;
            if (controller.viewType !== 'list' || isNestedModal) {
                return;
            }

            if (this.groupBySidebar) {
                this.groupBySidebar.destroy();
                this.groupBySidebar = null;
            }

            var isGrouped = listRenderer._isShowGroupBySidebar();
            if (isGrouped) {
                var self = this;
                this.groupBySidebar = new GroupBySidebar(this, listRenderer);
                this.groupBySidebar.appendTo(this.$el.find('.menu-wrapper')).then(function () {
                    self.groupBySidebar.selectFirstItem();
                    if (self.collapsed) {
                        self.$('.menu-wrapper [data-toggle="tooltip"]').tooltip();
                    }
                });

                this.doShowSidebar();

                // We do prioritize display groupby_sidebar menu over main sidebar menu
                // if groupby_sidebar is to be showed, the main sidebar menu will be hidden.
                if (this.$currentSidebar) {
                    this.$currentSidebar.addClass('o_hidden');
                    this.getParent().activeSidebarMenu(false);
                }
            } else if (this.$currentSidebar) {
                // if groupby_sidebar menu is not to be showed, then we'll show main sidebar menu if it's existed
                this.$currentSidebar.removeClass('o_hidden');
            } else {
                // otherwise we hide the sidebar
                this.doHideSidebar();
            }
        },

        start: function () {
            this.toggleCollapseState();
            this.subMenuVisibleState = {};

            var self = this;
            var promise = new Promise(function (resolve, reject) {
                var on_menu_item_click = function (ev) {
                    ev.preventDefault();
                    var menu_id = $(ev.currentTarget).data('menu');
                    var action_id = $(ev.currentTarget).data('action-id');
                    self._on_menu_click(menu_id, action_id);
                };
                self.$el.on('click', 'a[data-menu]', self, on_menu_item_click.bind(self));
                self.on_hashchange();

                self.$el
                    .on('mouseover', '.o_menu_lvl_2', self.onMouseOverMenuLv2.bind(self))
                    .on('mouseout', '.o_menu_lvl_2', self.onMouseOutMenuLv2.bind(self));
                resolve();
            });

            return promise;
        },

        onMouseOverMenuLv2: function (ev) {
            this.$el.find('.o_sub_menu_lvl_2').css({
                top: '',
                display: ''
            });

            var $menuLv2 = $(ev.currentTarget);
            var menuLv2Top = $menuLv2.offset().top;
            var $subMenuLv2 = $menuLv2.find('.o_sub_menu_lvl_2');
            $subMenuLv2.css({
                top: menuLv2Top,
                display: 'block'
            });
            this.subMenuVisibleState[ev.currentTarget] = true;
        },

        onMouseOutMenuLv2: function (ev) {
            this.subMenuVisibleState[ev.currentTarget] = false;

            var self = this;
            setTimeout(function hideSubmenu() {
                if (self.subMenuVisibleState[ev.currentTarget]) {
                    return;
                }

                var $menuLv2 = $(ev.currentTarget);
                var $subMenuLv2 = $menuLv2.find('.o_sub_menu_lvl_2');
                $subMenuLv2.css({
                    top: '',
                    display: ''
                });
            }, 150);
        },

        on_hashchange: function () {
            var currentState = $.bbq.getState();
            var action_id = currentState.action;
            var viewType = currentState.view_type;

            this.$('a[data-menu], .o_menu_header_lvl_2').removeClass('active');

            var $active_menu_item = this.$el.find('a[data-action-id="' + action_id + '"]');
            var sidebar_id = false;
            if ($active_menu_item.length > 0) {
                var $parent = $active_menu_item.closest('.sidebar_menu');
                sidebar_id = $parent.data('parent');
            }

            if (sidebar_id && viewType !== 'form') {
                $active_menu_item.addClass('active');

                var $closestMenuLv2 = $active_menu_item.closest('.o_menu_lvl_2');
                if ($closestMenuLv2.length) {
                    $closestMenuLv2.find('.o_menu_header_lvl_2').addClass('active');
                }

                this.openSidebar(sidebar_id, false);
            } else {
                this.closeSidebar(sidebar_id);
            }
        },
        _on_menu_click: function (menu_id, action_id) {
            this.trigger_up('menu_clicked', {
                id: menu_id,
                action_id: action_id,
                previous_menu_id: this.currentMenu || this.getParent().current_primary_menu,
            });
            this.currentMenu = menu_id
        },

        openSidebar: function (sidebar_id, openFirst) {
            var $sidebar = this.$('.sidebar_menu[data-parent="' + sidebar_id + '"]');
            if ($sidebar.length > 0) {
                this.$('.sidebar_menu[data-parent!="' + sidebar_id + '"]').addClass('o_hidden');
                if (openFirst) {
                    $sidebar.find('a[data-menu]').eq(0).trigger('click');
                    return;
                }

                this.currentSidebar = sidebar_id;
                this.$currentSidebar = $sidebar;
                this.doShowSidebar();
            }

            // Trigger active parent menu
            this.getParent().activeSidebarMenu(sidebar_id);
        },

        doShowSidebar: function () {
            if (this.$currentSidebar) {
                this.$currentSidebar.removeClass('o_hidden');
            }
            this.open = true;
            this.$mainContainer.addClass('sidebar-opened');
            this.toggleCollapseState();
            this.$el.show();
        },

        doHideSidebar: function () {
            this.open = false;
            this.$mainContainer.removeClass('sidebar-opened');
            if (this.$currentSidebar) {
                this.$currentSidebar.addClass('o_hidden');
            }
            this.$el.hide();
        },

        closeSidebar: function (sidebarId) {
            if (this.groupBySidebar) {
                return;
            }

            this.doHideSidebar();

            this.currentSidebar = false;
            this.$currentSidebar = false;
            this.getParent().activeSidebarMenu(false);
            this.getParent().activeHeaderMenu(sidebarId);
        }
    });

    return SidebarMenu;
});
