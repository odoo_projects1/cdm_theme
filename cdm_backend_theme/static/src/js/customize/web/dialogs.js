odoo.define('cdm_theme.view_dialogs', function (require) {
    "use strict";
    var Dialogs = require('web.view_dialogs');
    var Dialog = require('web.Dialog');
    var dom = require('web.dom');

    dom.renderButton = function (options) {
        var jQueryParams = _.extend({
            type: 'button',
        }, options.attrs || {});

        var extraClasses = jQueryParams.class;
        if (extraClasses) {
            // If we got extra classes, check if old oe_highlight/oe_link
            // classes are given and switch them to the right classes (those
            // classes have no style associated to them anymore).
            // TODO ideally this should be dropped at some point.
            extraClasses = extraClasses.replace(/\boe_highlight\b/g, 'btn-primary')
                .replace(/\boe_link\b/g, 'btn-link');
        }

        jQueryParams.class = 'btn';
        if (options.size) {
            jQueryParams.class += (' btn-' + options.size);
        }
        jQueryParams.class += (' ' + (extraClasses || 'btn-secondary'));

        var $button = $('<button/>', jQueryParams);

        var isCarbonIcon = false;

        if (options.icon) {
            if (options.icon.substr(0, 3) === 'fa-') {
                $button.append($('<i/>', {
                    class: 'fa fa-fw o_button_icon ' + options.icon,
                }));
                // Start custom icon
            } else if (options.icon.substr(0, 5) === 'icon-') {
                isCarbonIcon = true;
            } else {
                $button.append($('<img/>', {
                    src: options.icon,
                }));
            }
        }

        if (options.text) {
            $button.append($('<span/>', {
                text: options.text,
            }));
        }

        if (isCarbonIcon) {
            $button.append($('<span/>', {
                class: options.icon
            }));
        }

        return $button;
    };

    Dialogs.FormViewDialog.include({
        init: function () {
            this._super.apply(this, arguments);
            var self = this;

            _.forEach(this.buttons, function (button) {
                var isSaveButton = button.classes && button.classes.indexOf('btn-primary') > -1;
                if (isSaveButton) {
                    button.icon = self.readonly ? 'icon-edit' : 'icon-save';

                    return;
                }

                var isCancelButton = button.classes && button.classes.indexOf('o_form_button_cancel') > -1;
                if (isCancelButton) {
                    button.icon = 'icon-close'
                }
            });

            if (this.options.open_fullscreen) {
                this.fullscreen = true;
            }
        },

        destroy: function () {
            var parent = this.getParent();
            var isFormViewReloaded = this.form_view && this.form_view.isReloaded;

            this._super.apply(this, arguments);

            // If the Dialog FormView is reload
            // -> We'll reload the parent view after closing FormView Dialog to update info
            if (isFormViewReloaded && parent) {
                if (parent.reload) {
                    parent.reload();
                }
            }
        }
    });

    Dialogs.SelectCreateDialog.include({
        init: function () {
            this._super.apply(this, arguments);

            if (this.options.open_fullscreen) {
                this.fullscreen = true;
            }
        }
    });

    Dialog.include({
        init: function (parent, options) {
            this._super.apply(this, arguments);
            this.backupOptions = options;
        },

        destroy: function () {
            this.reloadSourceListController();
            this._super.apply(this, arguments);
        },

        reloadSourceListController: function () {
            try {
                var saveCounter = this.getParent().currentDialogController.widget.saveCounter;

                if (saveCounter) {
                    if (this.backupOptions.sourceListController) {
                        this.backupOptions.sourceListController.reload();
                    }

                    if (this.backupOptions.sourceMessageCenter) {
                        this.backupOptions.sourceMessageCenter.reloadCurrentThread();
                    }
                }

                parent = null;
            } catch (e) {
                // simply ignore this exception if we couldn't retrieve saveCounter
            }
        }
    })
});