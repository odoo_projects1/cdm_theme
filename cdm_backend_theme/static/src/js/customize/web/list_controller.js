odoo.define('cdm_theme.ListController', function (require) {
    "use strict";
    var ListController = require('web.ListController');

    ListController.include({
        init: function (parent, model, renderer, params) {
            this._super.apply(this, arguments);
            this._originalTitle = this._title;
        },

        start: function () {
            var self = this;
            this._super.apply(this, arguments).then(function () {
                self._originalPager = self.controlPanelElements.$pager;
            })
        },
        renderButtons: function () {
            this._super.apply(this, arguments); // Sets this.$buttons
            this._bindCustomButtonsReload();
        },

        _bindCustomButtonsReload: function () {
            if (!this.$buttons) {
                return;
            }

            var self = this;

            this.$buttons.on('click', '.o_button_reload_page', function () {
                self.update({});
            });
        },
    })
});
