odoo.define('cdm_them.ControlPanelRenderer', function (require) {
    var ControlPanelRenderer = require('web.ControlPanelRenderer');
    var CdmThemeSearchBarMenu = require('cdm_theme.SearchBarMenu');
    var SearchBar = require('web.SearchBar');
    var config = require('web.config');

    var FavoriteMenu = require('cdm_theme.FavoriteMenu');
    var FilterMenu = require('cdm_theme.FilterMenu');
    var GroupByMenu = require('cdm_theme.GroupByMenu');
    var TimeRangeMenu = require('cdm_theme.TimeRangeMenu');

    ControlPanelRenderer.include({
        custom_events: _.extend({}, ControlPanelRenderer.prototype.custom_events, {
            'on_switching_advanced_search_menu': '_onSwitchingAdvancedSearchMenu',
            'on_input_click': 'onSearchTriggered'
        }),
        init: function (parent, state, params) {
            this._super.apply(this, arguments);

            this.$headerSubMenus = null;
            this.headerMenusSetup = false;
            this.headerSubMenus = {};
            this.advancedSearchBoxOpened = false;
            this.$advancedSearchBox = $('nav.o_main_navbar .cdm_theme_advanced_search_box');

            this.hideAdvancedSearchHandler = this.handleHideAdvancedSearch.bind(this);
            this.registerOnClickEvents();
        },

        _renderBreadcrumbs: function () {
            this._super.apply(this, arguments);

            this.$el.find('li.breadcrumb-item').each(function (index, item) {
                var $item = $(item);

                $item.attr('title', $item.text());
            });
        },

        registerOnClickEvents: function () {
            $(document).on('click', 'body', this.hideAdvancedSearchHandler);
        },

        destroy: function () {
            $(document).off('click', 'body', this.hideAdvancedSearchHandler);

            this._super.apply(this, arguments);
        },

        _onSwitchingAdvancedSearchMenu: function (ev) {
            ev.stopPropagation();
            var filterBoxContainer = $('nav.o_main_navbar .filter_box_container');
            filterBoxContainer.find('button.btn').removeClass('active');
            filterBoxContainer.find('.o_dropdown_menu').removeClass('show');
        },

        handleHideAdvancedSearch: function (ev) {
            var $searchBoxWrapper = $(ev.target).closest('.cdm_theme_search_box');
            if ($searchBoxWrapper.length === 0 && this.$advancedSearchBox.css('display') !== 'none') {
                this.onSearchClosed();
            }
        },

        onSearchTriggered: function () {
            this.advancedSearchBoxOpened = true;
            this.$advancedSearchBox.fadeIn(100);
        },

        onSearchClosed: function () {
            this.advancedSearchBoxOpened = false;
            this.headerSearchBar && this.headerSearchBar.onInputBlur();
            this.$advancedSearchBox.fadeOut(100);
        },

        _inDialog: function () {
            return this.getParent()
                && this.getParent().getParent()
                && this.getParent().getParent().getParent()
                && this.getParent().getParent().getParent().backdrop;
        },

        _renderSearchBar: function () {
            var self = this;
            return this._super.apply(this, arguments).then(function () {
                var inDialog = self._inDialog();
                if (inDialog) {
                    return;
                }

                var oldSearchBar = self.headerSearchBar;
                var oldExtraSearchBar = self.extraSearchBar;
                if (oldSearchBar && oldSearchBar.$el) {
                    oldSearchBar.do_hide();
                }

                if (oldExtraSearchBar && oldExtraSearchBar.$el) {
                    oldExtraSearchBar.do_hide();
                }

                self.headerSearchBar = new CdmThemeSearchBarMenu(self, {
                    context: self.context,
                    facets: self.state.facets,
                    fields: self.state.fields,
                    filterFields: self.state.filterFields
                });

                // this search bar is used to render filter tags when ever
                // the advanced search box is shown
                self.extraSearchBar = new SearchBar(self, {
                    context: self.context,
                    facets: self.state.facets,
                    fields: self.state.fields,
                    filterFields: self.state.filterFields
                });

                if (oldSearchBar) {
                    var $headerSearchBar = $('nav.o_main_navbar .cdm_theme_search_bar');
                    var $extraSearchBar = $('nav.o_main_navbar .extra_search_bar');
                    $headerSearchBar.empty();
                    $extraSearchBar.empty();

                    var defs = [
                        self.headerSearchBar.appendTo($headerSearchBar),
                        self.extraSearchBar.appendTo($extraSearchBar)
                    ];

                    return Promise.all(defs).then(function () {
                        oldSearchBar.destroy();
                        oldExtraSearchBar.destroy();

                        self._doSetupOrUpdateHeaderMenus();
                    });
                }
            });
        },

        _focusSearchInput: function () {
            if (this._inDialog()) {
                this._super.apply(this, arguments);
            } else if (this.withSearchBar
                && !config.device.isMobile
                && this.headerSearchBar
                && this.headerSearchBar.$el
                && this.advancedSearchBoxOpened) { // only focus when the search box is opened
                // in mobile mode, we would rather not focus manually the
                // input, because it opens up the integrated keyboard, which is
                // not what you expect when you just selected a filter.
                this.headerSearchBar.focus();
            }
        },

        _getHeaderSubMenusPlace: function () {
            return $('<div class="filter_box_container"/>').appendTo($('nav.o_main_navbar .advanced_filter'));
        },

        _doSetupOrUpdateHeaderMenus: function () {
            if (this.headerMenusSetup) {
                this._updateHeaderMenus();
                return Promise.resolve();
            } else {
                this.headerMenusSetup = true;
                return this._setupHeaderMenus();
            }
        },

        _updateHeaderMenus: function () {
            var self = this;
            this.searchMenuTypes.forEach(function (menuType) {
                self.headerSubMenus[menuType].update(self._getMenuItems(menuType));
            });
        },

        _setupHeaderMenus: function () {
            this.$headerSubMenus = this._getHeaderSubMenusPlace();
            return this.searchMenuTypes.map(this._setupHeaderMenu.bind(this));
        },

        _setupHeaderMenu: function (menuType) {
            var Menu;
            var menu;
            if (menuType === 'filter') {
                Menu = FilterMenu;
            }
            if (menuType === 'groupBy') {
                Menu = GroupByMenu;
            }
            if (menuType === 'timeRange') {
                Menu = TimeRangeMenu;
            }
            if (menuType === 'favorite') {
                Menu = FavoriteMenu;
            }
            if (_.contains(['filter', 'groupBy', 'timeRange'], menuType)) {
                menu = new Menu(this, this._getMenuItems(menuType), this.state.fields);
            }
            if (menuType === 'favorite') {
                menu = new Menu(this, this._getMenuItems(menuType), this.action);
            }
            this.headerSubMenus[menuType] = menu;
            return menu.appendTo(this.$headerSubMenus);
        }
    });
});