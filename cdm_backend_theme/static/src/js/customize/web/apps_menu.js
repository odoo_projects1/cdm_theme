odoo.define('cdm_theme.AppsMenu', function (require) {
    "use strict";

    var AppsMenu = require('web.AppsMenu');
    AppsMenu.include({
        init: function (parent, menuData) {
            this._super.apply(this, arguments);

            var self = this;
            this._apps = _.map(menuData.children, function (appMenuData) {
                return {
                    actionID: parseInt(appMenuData.action.split(',')[1]),
                    menuID: appMenuData.id,
                    name: appMenuData.name,
                    xmlID: appMenuData.xmlid,
                    appIconUrl: self._getAppIconUrl(appMenuData['web_icon'])
                };
            });
        },

        _getAppIconUrl: function (webIcon) {
            if (webIcon) {
                return '/' + webIcon.replace(',', '/');
            }

            return '/base/static/description/settings.png';
        }
    })
});