odoo.define('cdm_theme.AttachDocument', function (require) {
    "use strict";
    var widgetRegistry = require('web.widget_registry');

    var AttachDocument = widgetRegistry.get('attach_document');
    if (!AttachDocument) {
        return;
    }

    AttachDocument.include({
        _notifyInvalidFields: function (invalidFields) {
            // we don't show invalid field warning anymore due to #1420
        }
    })
});