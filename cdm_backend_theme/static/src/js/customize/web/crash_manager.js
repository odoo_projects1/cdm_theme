odoo.define('cdm_theme.CrashManager', function (require) {
    "use strict";
    var CrashManager = require('web.CrashManager');
    var core = require('web.core');
    var crash_registry = core.crash_registry;
    var _t = core._t;

    function removeOdooInfo(error) {
        if (error.type) {
            error.type = error.type.replace(/Odoo/, '').trim()
        }
        if (error.message) {
            error.message = error.message.replace(/Odoo/, '').trim()
        }
    }

    CrashManager.CrashManager.include({
        show_error: function (error) {
            removeOdooInfo(error);
            return this._super.apply(this, arguments);
        },
        rpc_error: function (error) {
            removeOdooInfo(error);
            return this._super.apply(this, arguments);
        },
        show_warning: function (error, options) {
            removeOdooInfo(error);
            return this._super.apply(this, arguments);
        }
    });

    var RedirectWarning = crash_registry.get('odoo.exceptions.RedirectWarning');
    RedirectWarning.include({
        init: function (parent, error) {
            removeOdooInfo(error);
            this._super(parent, error);
        }
    });

    function session_expired(cm) {
        return {
            display: function () {
                cm.show_warning(
                    {
                        type: _t("Session Expired"),
                        data: {message: _t("Your session expired. Please login again.")}
                    },
                    {
                        buttons: [
                            {
                                text: _t("Ok"),
                                close: true,
                                click: function () {
                                    // reload the the page after user hit button ok
                                    window.location.reload(true);
                                }
                            }
                        ]
                    }
                );
            }
        }
    }

    core.crash_registry.add('odoo.http.SessionExpiredException', session_expired);
    core.crash_registry.add('werkzeug.exceptions.Forbidden', session_expired);
});