odoo.define('cdm_theme.GroupSearchFacet', function (require) {
    var SearchFacet = require('web.SearchFacet');
    var core = require('web.core');
    var _t = core._t;

    /**
     * This GroupSearchFacet is a placeholder for a combination of facets
     * since we don't have enough space to display all the facet on header search bar
     */
    var GroupSearchFacet = SearchFacet.extend({
        init: function (parent, facets) {
            this._super.apply(this, arguments);
            this.facets = facets;
            this.facetValues = ['+ ' + this.facets.length + _t(' filters')];
            this.separator = this._getSeparator();
            this.icon = this._getIcon();
            this._isComposing = false;
        },

        _getIcon: function () {
            return 'fa-tags';
        },

        _getSeparator: function () {
            return '';
        },

        _onFacetRemove: function () {
            this.trigger_up('facet_removed', {group: this.facets});
        }
    });

    return GroupSearchFacet;
});
