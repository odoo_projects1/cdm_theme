odoo.define('cdm_theme.Menu', function (require) {
    "use strict";

    var dom = require('web.dom');
    var Menu = require('web.Menu');
    var AppsMenu = require('web.AppsMenu');
    var SystrayMenu = require('web.SystrayMenu');
    var SidebarMenu = require('cdm_theme.SidebarMenu');

    Menu.include({
        sidebarMenuContents: false,
        sidebarMenu: false,
        activeSidebarId: false,
        init: function (parent, menu_data) {
            this.filterSidebarMenuContent(menu_data);

            if (menu_data.is_multilevel_menu) {
                this.menusTemplate = 'Cdm_theme.Menu.sections';
            }

            this._super.apply(this, arguments);
            $(window).bind('hashchange', this.on_hashchange);
        },

        start: function () {
            // Overwrite all parent start
            var self = this;

            this.$menu_apps = this.$('.o_menu_apps');
            this.$menu_brand_placeholder = this.$('.o_menu_brand');
            this.$section_placeholder = this.$('.o_menu_sections');

            if (this.menu_data.is_multilevel_menu) {
                this.$menu_apps.addClass('_underline');
            }

            // Navbar's menus event handlers
            var on_secondary_menu_click = function (ev) {
                ev.preventDefault();
                var menu_id = $(ev.currentTarget).data('menu');
                var action_id = $(ev.currentTarget).data('action-id');
                self._on_secondary_menu_click(menu_id, action_id);
            };
            var menu_ids = _.keys(this.$menu_sections);
            var primary_menu_id, $section;
            for (var i = 0; i < menu_ids.length; i++) {
                primary_menu_id = menu_ids[i];
                $section = this.$menu_sections[primary_menu_id];
                $section.on('click', 'a[data-menu]', self, on_secondary_menu_click.bind(this));
            }

            // Apps Menu
            this._appsMenu = new AppsMenu(self, this.menu_data);
            var appsMenuProm = this._appsMenu.appendTo(this.$menu_apps);

            // Systray Menu
            this.systray_menu = new SystrayMenu(this);
            var systrayMenuProm = this.systray_menu.attachTo(this.$('.o_menu_systray')).then(function () {
                dom.initAutoMoreMenu(self.$section_placeholder, {
                    // CDM Custom max width remove menu brand width, add search box width
                    maxWidth: function () {
                        return self.$el.width() - (self.$menu_apps.outerWidth(true) + self.systray_menu.$el.outerWidth(true) + self.$('.cdm_theme_search_box').outerWidth(true));
                    },
                    sizeClass: 'SM',
                });
            });

            // CDM Add sidebar
            var sidebarMenuProm = this.createSidebarMenus();

            return Promise.all([sidebarMenuProm, appsMenuProm, systrayMenuProm]);
        },
        on_hashchange: function () {
            // Detect active link
            var action_id = $.bbq.getState().action;
            var $active_menu_item = this.$el.find('a[data-action-id="' + action_id + '"]');
            this.$('a[data-menu], a.dropdown-toggle').removeClass('active');

            if (this.activeSidebarId) {
                $active_menu_item = this.$el.find('a[data-menu="' + this.activeSidebarId + '"]');
            }

            if ($active_menu_item.length > 0) {
                $active_menu_item.addClass('active');
                $active_menu_item.closest('.dropdown-menu').siblings('a.dropdown-toggle').addClass('active');
            }
        },

        activeHeaderMenu: function (menuId) {
            if (!menuId) {
                return;
            }

            var $active_menu = this.$el.find('a[data-menu="' + menuId + '"]');
            $active_menu.addClass('active');
            $active_menu.parent('.dropdown-menu').siblings('a.dropdown-toggle').addClass('active');
        },

        activeSidebarMenu: function (sidebarId) {
            var oldSidebarId = this.activeSidebarId;
            if (oldSidebarId) {
                var $inactive_sidebar_menu = this.$el.find('a[data-menu="' + this.activeSidebarId + '"]').removeClass('active');
                $inactive_sidebar_menu.removeClass('active');
                $inactive_sidebar_menu.closest('.dropdown-menu').siblings('a.dropdown-toggle').removeClass('active');
            }
            this.activeSidebarId = sidebarId;

            if (sidebarId) {
                var $active_sidebar_menu = this.$el.find('a[data-menu="' + sidebarId + '"]');
                $active_sidebar_menu.addClass('active');
                $active_sidebar_menu.parent('.dropdown-menu').siblings('a.dropdown-toggle').addClass('active');
            } else {
                this.on_hashchange();
            }
        },
        change_menu_section: function (primary_menu_id) {
            this._super.apply(this, arguments);
            this.on_hashchange();
        },
        filterSidebarMenuContent: function (menu_data) {
            // Filter menu data to create sidebar menu
            var menus_lv_0 = menu_data.children;
            var sidebarMenuContents = [];

            function splitSidebarContent(menu_obj) {
                if (menu_obj.children) {
                    if (menu_obj.children.length > 0 && menu_obj.web_icon === 'open_sidebar_menu') {
                        sidebarMenuContents.push({
                            name: menu_obj.name,
                            xmlid: menu_obj.xmlid,
                            children: menu_obj.children
                        });
                        menu_obj.id = menu_obj.xmlid;
                        menu_obj.action = ',open_sidebar_menu';
                        menu_obj.children = [];
                    }
                }
            }

            for (var i = 0; i < menus_lv_0.length; i++) {
                var menus_lv_1 = menus_lv_0[i].children || [];

                // Filter menu from level 2 - 3 only
                for (var j = 0; j < menus_lv_1.length; j++) {
                    var menus_lv_2 = menus_lv_1[j].children || [];
                    splitSidebarContent(menus_lv_1[j]);

                    for (var k = 0; k < menus_lv_2.length; k++) {
                        var menus_lv_3 = menus_lv_2[k].children || [];
                        splitSidebarContent(menus_lv_2[k]);

                        for (var l = 0; l < menus_lv_3.length; l++) {
                            splitSidebarContent(menus_lv_3[l]);
                        }
                    }
                }
            }

            this.sidebarMenuContents = sidebarMenuContents;
        },
        createSidebarMenus: function () {
            if (this.sidebarMenuContents) {
                this.sidebarMenu = new SidebarMenu(this, this.sidebarMenuContents);
                return this.sidebarMenu.prependTo($('#cdm_main_container'))
            }
            return Promise.resolve()
        },
        _on_secondary_menu_click: function (menu_id, action_id) {
            var self = this;
            // It is still possible that we don't have an action_id (for example, menu toggler)
            if (action_id) {
                if (action_id === 'open_sidebar_menu') {
                    if (this.sidebarMenu) {
                        this.sidebarMenu.openSidebar(menu_id, true);
                    }
                } else {
                    self._trigger_menu_clicked(menu_id, action_id);
                }
                this.current_secondary_menu = menu_id;
            }
        },

        _updateMenuBrand: function (brandName) {
            // CDM Custom hide menu brand
            if (brandName) {
                this.$menu_brand_placeholder.text(brandName).hide();
                this.$section_placeholder.show();
            } else {
                this.$menu_brand_placeholder.hide();
                this.$section_placeholder.hide();
            }
        },
    })
});
