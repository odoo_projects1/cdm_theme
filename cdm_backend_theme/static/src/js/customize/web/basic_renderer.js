odoo.define('cdm_theme.BasicRenderer', function (require) {
    "use strict";
    var BasicRenderer = require('web.BasicRenderer');
    BasicRenderer.include({
        isRequiredField: function (attrs, field) {
            var required = attrs.modifiers && attrs.modifiers.required;
            required = required || (attrs.modifiersValue && attrs.modifiersValue.required);
            required = required || field.required;

            return required === true; // must be a boolean, rather than other truthy values (arrays)
        },

        _renderFieldWidget: function (node, record, options) {
            var fieldName = node.attrs.name;
            var attrs = record.fieldsInfo[this.viewType][fieldName];
            var field = record.fields[fieldName];

            var required = this.isRequiredField(attrs, field);

            if (field.type === 'selection') {
                attrs.placeholder = '--' + (required ? ' *' : '');
            } else {
                attrs.placeholder = field.string + (required ? ' *' : '');
            }

            return this._super.apply(this, arguments);
        },

        _handleAttributes: function ($el, node) {
            if ($el.is('button')) {
                return;
            }

            var placeholder = $el.attr('placeholder');

            this._super.apply(this, arguments);

            if (node.attrs.placeholder && placeholder && _.string.endsWith(placeholder.trim(), '*')) {
                $el.attr('placeholder', node.attrs.placeholder + ' *');
            }
        }
    })
});