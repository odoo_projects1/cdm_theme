odoo.define('cdm_theme.ToggleSidebarMixins', function (require) {
    "use strict";
    var core = require('web.core');
    var _t = core._t;

    var ToggleSidebarMixins = {
        events: {
            'click .toggle-sidebar': 'toggleSidebar'
        },
        init: function () {
            this.$mainContainer = $('#cdm_main_container');
            this.collapsed = this.call('local_storage', 'getItem', 'sidebar_menu_collapsed') || false;
            this.toggleSidebarBtnContent = this.getToggleSidebarBtnContent();
        },
        toggleCollapseState: function () {
            var hasIcons = this.$('.sidebar_menu:not(.o_hidden) span.icon').length > 0;
            hasIcons = hasIcons || this.$('.side-menu span.icon').length > 0;

            var collapseClass = hasIcons ? 'sidebar-icons-collapsed' : 'sidebar-collapsed';

            this.$mainContainer.removeClass('sidebar-icons-collapsed sidebar-collapsed');
            this.$mainContainer.toggleClass(collapseClass, this.collapsed);
            this.enableTooltip();
        },

        enableTooltip: function(){
            if (this.collapsed) {
                this.$('.menu-wrapper .o_menu_entry_lvl_2[data-toggle="tooltip"]').tooltip('enable');
                this.$('.menu-item[data-toggle="tooltip"]').tooltip('enable');
            } else {
                this.$('.menu-wrapper .o_menu_entry_lvl_2[data-toggle="tooltip"]').tooltip('disable');
                this.$('.menu-item[data-toggle="tooltip"]').tooltip('disable');
            }
        },

        getToggleSidebarBtnContent: function () {
            return this.collapsed
                ? {icon: 'icon icon-page_last', text: _t('Expand')}
                : {icon: 'icon icon-page_first', text: _t('Collapse')};
        },
        toggleSidebar: function () {
            this.collapsed = !this.collapsed;

            this.toggleCollapseState();

            this.toggleSidebarBtnContent = this.getToggleSidebarBtnContent();
            this.$toggleSidebarBtn = this.$('.toggle-sidebar');
            this.$toggleSidebarBtn.find('.icon').removeClass().addClass(this.toggleSidebarBtnContent.icon);
            this.$toggleSidebarBtn.find('.text').text(this.toggleSidebarBtnContent.text);
            this.call('local_storage', 'setItem', 'sidebar_menu_collapsed', this.collapsed);
        }
    };

    return ToggleSidebarMixins;
});
