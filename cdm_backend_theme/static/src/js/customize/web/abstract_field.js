odoo.define('cdm_theme.AbstractField', function (require) {
    "use strict";

    var AbstractField = require('web.AbstractField');
    AbstractField.include({
        updateModifiersValue: function (modifiers) {
            this._super.apply(this, arguments);

            if (this.mode === 'edit') {
                var attrs = this.attrs;
                var placeholder = attrs.placeholder;
                var wasRequiried = _.string.endsWith(placeholder, ' *');
                var isRequired = !!modifiers.required;

                if (wasRequiried !== isRequired) {
                    if (isRequired) {
                        attrs.placeholder = placeholder + ' *';
                    } else if (wasRequiried) {
                        attrs.placeholder = placeholder.slice(0, -2);
                    }

                    if ('_setValues' in this) {
                        this._setValues();
                    }

                    this._renderEdit();

                    // Fix the case where _renderEdit didn't rerender the placeholder
                    // of relational fields
                    if (this.$input) {
                        var placeholeder = this.$input.attr('placeholder');
                        if (placeholeder !== attrs.placeholder) {
                            this.$input.attr('placeholder', attrs.placeholder);
                        }
                    }
                }
            }
        }
    });
});