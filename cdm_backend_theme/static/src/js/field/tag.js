odoo.define('cdm_theme.TagField', function (require) {
    "use strict";
    var basic_fields = require('web.basic_fields');
    var fieldRegistry = require('web.field_registry');
    var FieldChar = basic_fields.FieldChar;

    var TagField = FieldChar.extend({
        className: 'cdm_tag',
    });

    fieldRegistry.add('cdm_tag', TagField)
});