odoo.define('cdm_core.StatusBar', function (require) {
    "use strict";

    var core = require('web.core');
    var fieldRegistry = require('web.field_registry');
    var qweb = core.qweb;

    var FieldStatus = require('web.relational_fields').FieldStatus;
    var pyUtils = require('web.py_utils');

    var CDMFieldStatus = FieldStatus.extend({
        className: 'cdm_statusbar_status',
        events: {
            'click .status-btn': '_onClickStage',
        },
        decorations: [],
        specialData: "_fetchSpecialStatus",
        supportedFieldTypes: ['selection', 'many2one'],
        /**
         * @override init from AbstractField
         */
        init: function () {
            this._super.apply(this, arguments);
            this._setState();
            this.isClickable = !!this.attrs.clickable || !!this.nodeOptions.clickable;
            // this._searchDecorationFields();
        },

        //--------------------------------------------------------------------------
        // Private
        //--------------------------------------------------------------------------


        /**
         * @override _reset from AbstractField
         * @private
         */
        _reset: function () {
            this._super.apply(this, arguments);
            this._setState();
        },
        /**
         * Prepares the rendering data from the field and record data.
         * @private
         */
        _setState: function () {
            var self = this;
            if (this.field.type === 'many2one') {
                this.status_information = _.map(this.record.specialData[this.name], function (info) {
                    return _.extend({
                        selected: info.id === self.value.res_id,
                    }, info);
                });
                if (this.record.data.hasOwnProperty('approval_step_applied_ids')) {
                    var approval_step_applied_ids = this.record.data.approval_step_applied_ids.data;
                    var list_id = [];
                    approval_step_applied_ids.forEach(function (item) {
                        list_id.push(item.data.id);
                    })
                    this.status_information = _.reject(this.status_information, function (val) {
                        return !list_id.includes(val.id);
                    });
                }
            } else {
                var selection = this.field.selection;
                if (this.attrs.statusbar_visible) {
                    var restriction = this.attrs.statusbar_visible.split(",");
                    selection = _.filter(selection, function (val) {
                        return _.contains(restriction, val[0]) || val[0] === self.value;
                    });
                }
                this.status_information = _.map(selection, function (val) {
                    return {id: val[0], display_name: val[1], selected: val[0] === self.value, fold: false};
                });
            }
        },

        /**
         * @override _render from AbstractField
         * @private
         */
        _render: function () {
            this.$el.html(qweb.render("FieldCDMStatusBar.content", {
                selection_items: this.status_information,
                clickable: this.isClickable,
            }));

            if (this.attrs.decorations) {
                this._applyDecorations();
            }

            this.$('[data-toggle="tooltip"]').tooltip()
        },

        //--------------------------------------------------------------------------
        // Handlers
        //--------------------------------------------------------------------------

        /**
         * Called when on status stage is clicked -> sets the field value.
         *
         * @private
         * @param {MouseEvent} e
         */
        _onClickStage: function (e) {
            if (this.isClickable) {
                this._setValue($(e.currentTarget).data("value"));
            }
        }
    });

    fieldRegistry.add('cdm_statusbar', CDMFieldStatus)
});
