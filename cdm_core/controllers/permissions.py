from odoo import http
from odoo.http import request


class Permissions(http.Controller):
    def get_permissions(self):
        return {
            'is_admin': request.env.is_admin(),
            'is_system': request.env.is_system()
        }

    @http.route('/cdm_core/permissions', auth='user', type='json')
    def get_group_permissions(self):
        return self.get_permissions()
