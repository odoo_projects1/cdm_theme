// Add action to render lottery result from internet via lottery_result_rest.py controller
odoo.define('cdm_core.UserMenu', function (require) {
    "use strict";
    var UserMenu = require('web.UserMenu');
    var core = require('web.core');
    var _t = core._t;

    UserMenu.include({
        start: function () {
            var self = this;
            this._super.apply(this, arguments);
            var session = this.getSession();
            if (session.require_change_password) {
                setTimeout(function () {
                    self.do_action({
                        name: _t('Change Password'),
                        type: 'ir.actions.client',
                        tag: 'change_password',
                        context: {require_change_password: true},
                        target: 'new'
                    })
                }, 2000);
            }
        }
    })
});
