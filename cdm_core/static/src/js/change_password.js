// Add action to render lottery result from internet via lottery_result_rest.py controller
odoo.define('cdm_core.ChangePassword', function (require) {
    "use strict";
    var ChangePassword = require('web.ChangePassword');
    var web_client = require('web.web_client');
    var core = require('web.core');
    var _t = core._t;

    ChangePassword.include({
        init: function (parent, action, options) {
            this.parent = parent;
            this.action = action;
            this.options = options;
            this._super.apply(this, arguments);
        },
        start: function () {
            if (this.action.context && this.action.context.require_change_password) {
                this.$('.o_inner_group').before('<div class="mb-3">' + _t('You are using default password delivered by admin. Because security reason your need to change password.') + '</div>');
                this.$('.o_inner_group').after('<div class="mb-3">' + _t('<b>Note: </b>After change password successfully, you need to login again.') + '</div>')
            } else {
                web_client.set_title(_t("Change Password"));
            }
        },
        renderButtons: function ($footer) {
            var self = this;
            var $button = this.$('.oe_form_button');
            $button.appendTo($footer);
            $button.eq(1).click(function () {
                self.$el.parents('.modal').modal('hide');
            });
            $button.eq(0).click(function () {
                self._rpc({
                    route: '/web/session/change_password',
                    params: {
                        fields: $('form[name=change_password_form]').serializeArray()
                    }
                }).then(function (result) {
                        if (result.error) {
                            self._display_error(result);
                        } else {
                            self.do_action('logout');
                        }
                    });
            });
        }
    })
});
