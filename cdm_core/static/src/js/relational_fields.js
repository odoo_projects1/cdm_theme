odoo.define('cdm_core.relational_fields', function (require) {
    "use strict";
    var FieldMany2One = require('web.relational_fields').FieldMany2One;

    FieldMany2One.include({
        _renderReadonly: function () {
            this._super.apply(this, arguments);
            var isOpenPopup = this.attrs.popup;
            var isOpenNewTab = this.attrs.newtab;

            if (isOpenPopup || isOpenNewTab || !this.value) {
                this.$el.attr('href', 'javascript:void(0);');
            }
        },
        openPopup: function () {
            var fullscreen = !!this.attrs.fullscreen;
            var targetRes = this.recordData[this.name];

            if (!targetRes) {
                return;
            }

            var action = {
                type: 'ir.actions.act_window',
                name: this.m2o_value,
                res_model: targetRes.model,
                res_id: targetRes.res_id,
                views: [[false, 'form']],
                target: 'new',
                flags: {
                    mode: 'readonly'
                },
                updateFromView: true
            };

            this.do_action(action, {
                fullscreen: fullscreen
            });
        },

        openNewTab: function () {
            var targetRes = this.recordData[this.name];

            if (!targetRes) {
                return;
            }

            var currentState = $.bbq.getState();
            var menuId = currentState.menu_id;
            var viewType = (this.recordParams && this.recordParams.viewType) || 'form';
            var resModel = targetRes.model;
            var resId = targetRes.res_id;

            var _hash = _.str.sprintf('id=%s&model=%s&view_type=%s&menu_id=%s', resId, resModel, viewType, menuId);

            window.open('/web#' + _hash);
        },

        _onClick: function (event) {
            if (this.mode === 'readonly' && !this.noOpen) {
                var targetRes = this.recordData[this.name];
                if (targetRes) {
                    event.preventDefault();
                    event.stopPropagation();

                    var isOpenPopup = this.attrs.popup;
                    var isOpenNewTab = this.attrs.newtab;

                    if (isOpenPopup) {
                        if (this._inDialog()) {
                            this.openNewTab();
                            return;
                        } else {
                            this.openPopup();
                            return;
                        }
                    } else if (isOpenNewTab) {
                        this.openNewTab();
                        return;
                    }
                }
            }

            if (!this.value){
                return;
            }

            this._super.apply(this, arguments);
        },

        _inDialog: function () {
            if (this.getParent()
                && this.getParent().getParent()
                && this.getParent().getParent().getParent()
                && this.getParent().getParent().getParent()){
                var parent = this.getParent().getParent().getParent();
                if (parent.currentDialogController){
                    return true;
                }
                if (parent.context && parent.context.hasOwnProperty('isPopup') && parent.context.isPopup){
                    return true;
                }
            }
            return false;
        },
    });

});
