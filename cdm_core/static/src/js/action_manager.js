odoo.define('cdm_core.ActionManager', function (require) {
    var ActionManager = require('web.ActionManager');
    var viewDialogs = require('web.view_dialogs');
    var core = require('web.core');
    var _t = core._t;

    ActionManager.include({
        changeZIndexBackdropModal: function () {
            var zIndex = 1040;
            $('.modal-backdrop').each(function () {
                $(this).css({'z-index': zIndex})
                zIndex = 1050;
            })
        },
        _executeCloseAction: function (action, options) {
            if (action.notify) {
                this.call('notification', 'notify', {
                    type: action.notify_type,
                    title: action.title,
                    message: action.message,
                });
            }
            return this._super.apply(this, arguments);
        },

        _handleAction: function (action, options) {
            if (action.type === 'ir.actions.client_service') {
                return this._executeClientService(action, options);
            }
            return this._super.apply(this, arguments);
        },
        _onOpenModalRead: function (dialogParent, action) {
            var record = dialogParent.form_view.model.get(dialogParent.form_view.handle);

            var self = this;
            var modal = new viewDialogs.FormViewDialog(this.getCurrentViewController(), {
                context: action.context,
                domain: action.domain,
                model: this.getCurrentController().widget.model,
                readonly: true,
                res_id: (record && record.res_id) || action.res_id,
                res_model: action.res_model,
                buttons: [
                    {
                        text: _t("Edit"),
                        classes: "btn-primary",
                        click: function () {
                            action.name = record.data && record.data.display_name; // customize title name
                            self.doActionFormViewDialog(action).then(function () {
                                modal.destroy();
                            });
                        }
                    },
                    {
                        text: _t("Close"),
                        classes: "btn-secondary o_form_button_cancel",
                        close: true,
                        click: function () {
                            modal.destroy();
                        },
                    }
                ],
                title: (record.data && record.data.display_name) || (_t("Open: ") + action.name),
            });

            modal.fullscreen = action.fullscreen;
            this.currentDialog = modal;
            return modal.open().opened().then(function () {
                dialogParent.destroy();
            });
        },

        getEditFormTitle: function (action) {
            var action_title = '';
            if (action.res_id) {
                action_title = (_.string.startsWith(action.name, _t("Edit")) ? '' : _t("Edit: "));
            } else {
                action_title = (_.string.startsWith(action.name, _t("Create")) ? '' : _t("Create: "));
            }

            if (action.hasOwnProperty('no_action_title')) {
                action_title = '';
            }

            return action_title + action.name;
        },

        afterSaveRecord: function (action) {

        },

        _doSaveRecord: function (dialog, action) {
            var self = this;
            dialog._save().then(function () {
                dialog.form_view.update({}).then(function () {
                    var modalCloseAfterSave = action.modal_close_after_save;

                    if (modalCloseAfterSave) {
                        dialog.destroy();
                    } else {
                        var formView = dialog.form_view;
                        var record = formView.model.get(formView.handle);
                        action.res_id = record && record.res_id;

                        self._onOpenModalRead(dialog, action);
                    }

                    if (dialog.hasOwnProperty('parentCurrentDialog')) {
                        var parentCurrentDialog = dialog.parentCurrentDialog;
                        if (parentCurrentDialog && parentCurrentDialog.form_view && !parentCurrentDialog.isDestroyed()) {
                            parentCurrentDialog.form_view.update({});
                        }
                    }

                    var currentController = self.getCurrentViewController();
                    if (currentController.update) {
                        currentController.update({});
                    }
                    if (currentController.hasOwnProperty('currentDialog') && currentController.currentDialog) {
                        var formViewParent = currentController.currentDialog.form_view;
                        if (formViewParent.hasOwnProperty('currentDialog') && formViewParent.currentDialog && !formViewParent.currentDialog.isDestroyed()) {
                            formViewParent.currentDialog.form_view.update({});
                        }
                        if (!currentController.currentDialog.isDestroyed()) {
                            formViewParent.update({});
                        }
                    }

                    self.afterSaveRecord(action)
                });
            });
        },

        getCurrentViewController: function () {
            var currentController = this.currentDialogController || this.getCurrentController();
            return currentController.widget;
        },

        doActionFormViewDialog: function (action) {
            var self = this;
            var dialog = new viewDialogs.FormViewDialog(this.getCurrentViewController(), {
                res_id: action.res_id || false,
                res_model: action.res_model,
                context: action.context,
                view_id: action.view_id || false,
                title: self.getEditFormTitle(action),
                buttons: [
                    {
                        text: _t("Save"),
                        classes: "btn-primary",
                        click: function () {
                            self._doSaveRecord(dialog, action);
                        }
                    },
                    {
                        text: _t("Discard"),
                        classes: "btn-secondary o_form_button_cancel",
                        click: function () {
                            var close = false;
                            if (action.hasOwnProperty('modal_close_after_cancel')) {
                                close = action.modal_close_after_cancel;
                            }
                            if (action.res_id && !close) {
                                self._onOpenModalRead(dialog, action);
                            } else {
                                dialog.destroy();
                            }
                        },
                    }
                ],
            });

            dialog.fullscreen = action.fullscreen;
            if (action.hasOwnProperty('modal_class')) {
                var dialogClass = 'o_act_window ' + action.modal_class;
                dialog.dialogClass = dialogClass;
            }
            if (this.hasOwnProperty('currentDialog')) {
                dialog.parentCurrentDialog = this.currentDialog;
            }
            dialog.currentViewController = this.getCurrentViewController();
            var modalsLength = $('body > .modal').filter(':visible').length;
            return dialog.open().opened().then(function () {
                if (action.hasOwnProperty('close_modal_parent') && modalsLength === 1) {
                    var modals = $('body > .modal').filter(':visible');
                    var modal_backdrop = $('body > .modal-backdrop').filter(':visible');
                    modals.first().remove();
                    modal_backdrop.first().remove();
                }
            });
        },

        _executeWindowAction: function (action, options) {
            if (action.modal_type && action.modal_type === 'FormViewDialog') {
                return this.doActionFormViewDialog(action, options);
            }

            if (action.fullscreen) {
                options.fullscreen = true;
            }

            return this._super.apply(this, arguments);
        },

        _executeClientService: function (action, options) {
            if (action.service && action.method) {
                var result;
                this.trigger_up('call_service', {
                    service: action.service,
                    method: action.method,
                    args: action.params,
                    callback: function (r) {
                        result = r;
                    }
                });
            }
        }
    });

});
