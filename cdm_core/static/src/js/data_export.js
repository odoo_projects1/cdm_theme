odoo.define('cdm_core.DataExport', function (require) {
    "use strict";
    var DataExport = require('web.DataExport');

    DataExport.include({
        init: function (parent, record, defaultExportFields, groupedBy, activeDomain, idsToExport) {
            this._super.apply(this, arguments);
            if (!this.idsToExport) {
                this.idsToExport = idsToExport;
            }
        },
    });

});
