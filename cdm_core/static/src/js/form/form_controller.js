odoo.define('cdm_core.FormController', function (require) {
    "use strict";

    var FormController = require('web.FormController');
    var core = require('web.core');
    var _t = core._t;

    FormController.include({

        init: function (parent, model, renderer, params) {
            this._super.apply(this, arguments);
            this.parent = parent;
            this.currentController = this.getCurrentController(parent);
        },

        updateDialogTitleAfterCreateOrUpdate: function () {
            var parent = this.getParent();
            if (!parent || !parent.hasOwnProperty('currentDialogController')) {
                return;
            }

            var currentDialogController = parent.currentDialogController;
            if (!currentDialogController) {
                return;
            }

            var isInDialog = currentDialogController && currentDialogController.dialog;
            if (isInDialog && this.getTitle()) {
                var title = this.getTitle();
                var changeTitle = true;
                if (this.hasOwnProperty('currentController') && this.currentController) {
                    var currentController = this.currentController.widget
                    if (currentController && currentController.hasOwnProperty('noChangeTitle') && currentController.noChangeTitle) {
                        changeTitle = false;
                    }
                    if (currentController && currentController.hasOwnProperty('titleDialogAfterSave') && currentController.titleDialogAfterSave) {
                        changeTitle = true;
                        title = currentController.titleDialogAfterSave;
                    }
                }
                if (changeTitle) {
                    currentDialogController.dialog.set_title(title);
                }
            }
        },

        update: function (params, options) {
            this.updateDialogTitleAfterCreateOrUpdate();

            return this._super.apply(this, arguments);
        },

        _saveRecord: function (recordId) {
            var self = this;
            return this._super.apply(this, arguments).then(function (res) {
                if (self.isUpdateFromView()) {
                    self.currentController.widget.update({});
                }
                if (self.isPopupMode()) {
                    self.closeDialog(res.length > 0);
                }
                return res;
            });
        },

        isUpdateFromView: function () {
            var actions = this.parent.actions;
            for (var key in actions) {
                var action = actions[key];
                if (action.updateFromView) {
                    return true;
                }
            }
            return false;
        },

        isPopupMode: function () {
            var actions = this.parent.actions;
            for (var key in actions) {
                var action = actions[key];
                if (action.popupMode) {
                    return true;
                }
            }
            return false;
        },

        closeDialog: function (edit) {
            if (this.currentController && edit) {
                this.currentController.widget.update({orderedBy: [{name: 'write_date', asc: false}]});
                this.call('notification', 'notify', {
                    type: 'success',
                    title: _t('Update'),
                    message: _t('Update successful'),
                });
            }
            this.do_action({type: 'ir.actions.act_window_close'});
        },

        _discardChanges: function () {
            if (this.isPopupMode()) {
                this.do_action({type: 'ir.actions.act_window_close'});
            }
            return this._super.apply(this, arguments);
        },

        getCurrentController: function (parent) {
            var currentControllerID = _.last(parent.controllerStack);
            return currentControllerID ? parent.controllers[currentControllerID] : null;
        },
    });
});
