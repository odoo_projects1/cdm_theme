odoo.define('cdm_core.FormRenderer', function (require) {
    "use strict";

    var FormRenderer = require('web.FormRenderer');

    FormRenderer.include({
        _renderTabHeader: function (page, page_id) {
            var tabLabel = page.attrs.string;

            var $a = $('<a>', {
                'data-toggle': 'tab',
                disable_anchor: 'true',
                href: '#' + page_id,
                class: 'nav-link',
                role: 'tab',
                text: tabLabel,
            });

            // Add follow field
            var followUpField = page.attrs.follow_up_field;
            var state = this.state;
            if (followUpField && state.fields.hasOwnProperty(followUpField)) {
                var field = state.fields[followUpField];
                if (['integer', 'float', 'char'].indexOf(field.type) > -1) {
                    var node = {
                        tag: 'field',
                        attrs: {
                            name: followUpField,
                            modifiers: {readonly: true}
                        }
                    };
                    var $el = this._renderFieldWidget(node, this.state);
                    $a.append(' (');
                    $a.append($el);
                    $a.append(')');
                }
            }

            // Add prefix icon
            var iconClass = page.attrs.icon_class;
            if (iconClass) {
                var $i = $('<span>', {
                    class: 'tab-icon ' + iconClass
                });
                $a.prepend($i)
            }

            return $('<li>', {class: 'nav-item'}).append($a);
        },
        _renderInnerGroupField: function (node) {
            var $td = this._super.apply(this, arguments);

            var field = this.state.fields[node.attrs.name];
            var fieldType = field.type;

            if (fieldType === 'boolean') {
                node.attrs.checkBoxShowLabel = node.attrs.nolabel !== '1';

                if ($td.length > 1) {
                    $td.first().css('display', 'none');
                }
            }

            return $td;
        }
    })
});