odoo.define('cdm_core.basic_fields', function (require) {
    "use strict";
    var BasicFields = require('web.basic_fields');
    BasicFields.FieldBoolean.include({
        _render: function () {
            this._super.apply(this, arguments);

            var nodeOptions = this.__node;
            if (nodeOptions && nodeOptions.attrs.checkBoxShowLabel) {
                this.$el.find('label').text(this.string);
            }
        }
    });
});