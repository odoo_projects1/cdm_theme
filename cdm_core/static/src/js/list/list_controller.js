odoo.define('cdm_core.ListController', function (require) {
    "use strict";
    var ListController = require('web.ListController');
    var Dialog = require('web.Dialog');

    var core = require('web.core');
    var _t = core._t;


    ListController.include({
        popupMode: false,
        popupFullwidth: false,
        noChangeTitle: false,
        init: function (parent, model, renderer, params) {
            this._super.apply(this, arguments);
            this.parent = parent;
            var class_str = renderer.arch.attrs.class || '';
            this.popupMode = class_str.indexOf('popup_mode') >= 0;
            this.noChangeTitle = class_str.indexOf('no_change_title') >= 0;
            this._doToggleAllowSelectAllRecords(false);
        },

        isPopupMode: function () {
            return this.popupMode;
        },

        _onOpenRecord: function (ev) {
            if (this.isPopupMode()) {
                var record = this.model.get(ev.data.id, {raw: true});
                var name = 'name';
                if (ev.target && ev.target.hasOwnProperty('arch') && ev.target.arch.hasOwnProperty('attrs')) {
                    var attrs = ev.target.arch.attrs;
                    if (attrs.hasOwnProperty('string') && this.noChangeTitle) {
                        this.titleDialogAfterSave = attrs.string;
                    }
                    if (attrs.hasOwnProperty('name')){
                        name = attrs.name;
                    }
                }
                this._openPopupRecord(record.data.id, record.data[name]);
            } else {
                this._super.apply(this, arguments);
            }

        },

        _onCreateRecord: function (ev) {
            if (this.isPopupMode()) {
                if (this.renderer && this.renderer.hasOwnProperty('arch') && this.renderer.arch.hasOwnProperty('attrs')) {
                    var attrs = this.renderer.arch.attrs;
                    if (attrs.hasOwnProperty('string') && this.noChangeTitle) {
                        this.titleDialogAfterSave = attrs.string;
                    }
                }
                this._openPopupRecord(null, _t("Create new"));
            } else {
                this._super.apply(this, arguments);
            }
        },

        _openPopupRecord: function (res_id, title) {
            var initialState = {};
            if (this.hasOwnProperty('initialState') && this.initialState.hasOwnProperty('context')) {
                initialState = this.initialState.context;
            }
            var context = _.extend({}, initialState, {
                form_view_initial_mode: res_id ? 'readonly' : 'edit'
            });
            var action = {
                name: title || this._title,
                type: 'ir.actions.act_window',
                views: [[false, 'form']],
                target: 'new',
                res_model: this.modelName,
                res_id: res_id,
                context: context
            };

            this.do_action(action, {
                fullscreen: this.popupFullwidth,
                sourceListController: this // attach source list controller for reloading if any saving is performed on the popup
            })
        },

        //=====================
        // Render Bulk Actions
        //=====================
        _getHeaderActions: function () {
            var actions = this._getDefaultActions();
            actions = actions.concat(this._getToolbarActions());
            actions = actions.concat(this._getTaskActions());

            return actions;
        },

        _doToggleAllowSelectAllRecords: function (allow) {
            this.allRecordsIds = null;
            this.allowSelectAllRecordsEnabled = allow;
        },

        _doSelectAllRecords: function () {
            var renderer = this.renderer;
            var targetDomain = renderer._isShowGroupBySidebar() ? renderer.toShowGroup.domain : renderer.state.domain;
            var orderBy = renderer._isShowGroupBySidebar() ? renderer.toShowGroup.orderedBy : renderer.state.orderedBy;

            var self = this;
            this._rpc({
                route: '/web/dataset/search_read',
                params: {
                    model: this.modelName,
                    domain: targetDomain,
                    fields: [
                        'id'
                    ],
                    limit: 1000,
                    orderBy: orderBy
                }
            }).then(function (result) {
                self.allRecordsIds = _.map(result.records, 'id');
                renderer._updateSelection();
            });
        },

        _doCancelSelectAllRecords: function () {
            this.allRecordsIds = null;
            this.renderer._updateSelection();
        },

        getSelectedIds: function () {
            if (this.allRecordsIds) {
                return this.allRecordsIds;
            }

            return this._super.apply(this, arguments);
        },

        _getCurrentPagerSize: function () {
            var renderer = this.renderer;
            if (renderer.isGrouped) {
                if (renderer._isShowGroupBySidebar()) {
                    return renderer.groupPager.state.size;
                }
            } else {
                return this.pager.state.size;
            }
        },

        canShowSelectAllRecordsButton: function () {
            var renderer = this.renderer;
            return renderer.isGrouped ? renderer._isShowGroupBySidebar() : true;
        },

        _getDefaultActions: function () {
            var self = this;
            var actions = [];
            if (this.allowSelectAllRecordsEnabled && this.canShowSelectAllRecordsButton()) {
                var totalSize = this._getCurrentPagerSize();
                if (totalSize && totalSize <= 1000 && totalSize > this.selectedRecords.length) {
                    if (this.allRecordsIds) {
                        actions.push({
                            name: _t("Select records in current page"),
                            action_id: 'default.select_all_records',
                            callback: this._doCancelSelectAllRecords.bind(this)
                        });
                    } else {
                        actions.push({
                            name: _t("Select all records"),
                            action_id: 'default.select_all_records',
                            callback: this._doSelectAllRecords.bind(this)
                        });
                    }

                }
            }

            if (this.is_action_enabled('export_xlsx')) {
                actions.push({
                    name: _t("Export"),
                    action_id: 'default.export',
                    callback: this._onExportData.bind(this)
                })
            }

            if (this.archiveEnabled) {
                actions.push({
                    name: _t("Archive"),
                    action_id: 'default.archive',
                    callback: function () {
                        Dialog.confirm(self, _t("Are you sure that you want to archive all the selected records?"), {
                            confirm_callback: self._toggleArchiveState.bind(self, true),
                        });
                    }
                });
                actions.push({
                    name: _t("Unarchive"),
                    action_id: 'default.unarchive',
                    callback: this._toggleArchiveState.bind(this, false)
                });
            }

            if (this.is_action_enabled('delete')) {
                actions.push({
                    name: _t('Delete'),
                    action_id: 'default.delete',
                    callback: this._onDeleteSelectedRecords.bind(this)
                });
            }

            return actions;
        },

        _getToolbarActions: function () {
            var toolbarActions = this.toolbarActions.action || [];
            if (toolbarActions && toolbarActions.length > 0) {
                toolbarActions = toolbarActions.map(function (action) {
                    return {
                        name: action.name,
                        action: action,
                        action_id: action.xml_id
                    }
                })
            }

            return toolbarActions;
        },

        _archive: function (ids, archive) {
            if (this.allRecordsIds && this.allRecordsIds.length > 0) {
                return this._archiveAllRecordIds(archive);
            }

            return this._super.apply(this, arguments);
        },

        _archiveAllRecordIds: function (archive) {
            if (archive) {
                return this.model
                    .actionArchiveByResIds(this.allRecordsIds, this.handle)
                    .then(this.update.bind(this, {}, {reload: false}));
            } else {
                return this.model
                    .actionUnarchiveByResIds(this.allRecordsIds, this.handle)
                    .then(this.update.bind(this, {}, {reload: false}));
            }
        },

        _deleteAllRecordIds: function (ids) {
            var self = this;
            var doIt = function () {
                return self.model
                    .deleteRecordsByResIds(ids, self.allRecordsIds, self.modelName)
                    .then(self._onDeletedRecords.bind(self, ids));
            };

            if (this.confirmOnDelete) {
                Dialog.confirm(this, _t("Are you sure you want to delete this record ?"), {
                    confirm_callback: doIt,
                });
            } else {
                doIt();
            }
        },

        _deleteRecords: function (ids) {
            if (this.allRecordsIds && this.allRecordsIds.length > 0) {
                this._deleteAllRecordIds(ids);
            } else {
                this._super.apply(this, arguments);
            }
        },
    })
});
