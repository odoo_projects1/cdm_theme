odoo.define('cdm_core.ListRenderer', function (require) {
    "use strict";

    var ListRenderer = require('web.ListRenderer');
    var HeaderActions = require('cdm_core.HeaderActions');

    ListRenderer.include({
        _renderView: function () {
            var listController = this.getParent();
            if (listController) {
                listController.$el.addClass('o_list_controller');
                if (!listController.bulkEnabled) {
                    this.hasSelectors = false;
                }
            }

            // remove list render margin-top style if there is any customization before
            this.$el.css('margin-top', '');

            return this._super.apply(this, arguments);
        },

        _onRowClicked: function (ev) {
            if (this.mode === 'readonly' && this.arch.attrs.no_open && !this.editable) {
                return false;
            } else {
                return this._super.apply(this, arguments);
            }
        },

        _renderHeader: function () {
            var $thread = this._super.apply(this, arguments);

            if (this.hasSelectors) {
                var $tr = this._getHeaderActionsRow();
                $thread.append($tr);
            }

            if (this.optionalColumns.length > 0) {
                $thread.find('tr:first th:last').append($('<i class="o_optional_columns_dropdown_toggle fa fa-ellipsis-v"/>'));
            }


            return $thread;
        },

        _onSortColumn: function (ev) {
            if (ev.target.className.indexOf('o_column_sortable') < 0)
                return;
            this._super.apply(this, arguments);
        },

        _updateSelection: function () {
            this._super.apply(this, arguments);
            var listController = this.getParent();
            if (listController.bulkEnabled) {
                this._renderHeaderActions();
                this._updatePartialSelectionStatus();
            }
        },

        _getHeaderActionsRow: function () {
            var $tr = $('<div class="actions-row">');
            $tr.append(this._renderSelector('th'));

            var colspan = this.columns.length;
            var $headerActionsTh = $('<th class="action-cell" colspan="' + colspan + '"/>');

            var listController = this.getParent();
            this.headerActions = new HeaderActions(this, listController._getHeaderActions());
            this.headerActions.appendTo($headerActionsTh);

            $tr.append($headerActionsTh);

            return $tr;
        },

        _renderHeaderActions: function () {
            if (this.headerActions) {
                var hasSelection = this.selection.length > 0;
                var actionsHeader = this.$el.find('thead div.actions-row');

                var firstHeadOuterHeight = this.$el.find('thead tr:first').outerHeight();

                // this seem to be a crazy hack, but it solves the problem :)
                if (hasSelection) {
                    this.$el.css('margin-top', -firstHeadOuterHeight);
                } else {
                    this.$el.css('margin-top', '');
                }

                actionsHeader.toggleClass('on-selected', hasSelection);

                var listController = this.getParent();
                this.headerActions.setActions(listController._getHeaderActions());
                this.headerActions.renderElement();
            }
        },

        _updatePartialSelectionStatus: function () {
            var $input = this.$('thead .o_list_record_selector input');
            if (!$input[0])
                return;
            var isPartialSelected = !$input[0].checked && this.selection.length > 0;
            var $headSelector = this.$('thead .o_list_record_selector .custom-checkbox');

            if (isPartialSelected) {
                $headSelector.addClass('partial-selected');
                $input.prop('checked', true);
            } else {
                $headSelector.removeClass('partial-selected');
            }
        },

        _onToggleSelection: function (ev) {
            var checked = $(ev.currentTarget).prop('checked') || false;
            var listController = this.getParent();
            listController._doToggleAllowSelectAllRecords(checked);

            this._super.apply(this, arguments);
        }
    });
});
