odoo.define('cdm_core.HeaderActions', function (require) {
    "use strict";

    var Widget = require('web.Widget');

    var HeaderActions = Widget.extend({
        template: 'HeaderActions',
        events: {
            'click .header-action': 'doTriggerAction',
            'click .dropdown-item': 'doTriggerAction'
        },
        init: function (parent, actions) {
            this._super.apply(this, arguments);
            this.headerActions = actions;
            this.shownActionIcon = {
                'default.select_all_records': 'icon-list_checked',
                'default.export': 'icon-download',
                'default.delete': 'icon-artboard-1',
                'cdm_crm.action_batch_create_opportunities': 'icon-star',
                'cdm_crm.task.mark_as_completed': 'icon-checkmart',
                'cdm_crm.task.mark_as_canceled': 'icon-error'
            }
        },
        setActions: function (actions) {
            this.headerActions = actions;
        },
        doTriggerAction: function (e) {
            e.preventDefault();

            var $target = $(e.currentTarget);
            var action = $target.data('action');

            var headerActions = this.headerActions;
            var targetAction = _.find(headerActions, function (_action) {
                return _action.action_id === action;
            });

            if (!targetAction) {
                return;
            }

            if (targetAction.callback) {
                targetAction.callback.apply(this, [targetAction]);
            } else if (targetAction.action) {
                var listController = this.getParent().getParent();
                if (listController && listController.sidebar) {
                    listController.sidebar._onItemActionClicked(targetAction);
                }
            }
        },
        getNumberOfSelectedRecords: function () {
            var listController = this.getParent().getParent();
            var numberOfSelectedRecords = listController.getSelectedIds().length;

            if (numberOfSelectedRecords < 10) {
                return '0' + numberOfSelectedRecords;
            }

            return String(numberOfSelectedRecords);
        },

        getShownActions: function () {
            var self = this;
            return this.headerActions
                .filter(function (action) {
                    return self.shownActionIcon[action.action_id];
                }).map(function (action) {
                    action.icon = self.shownActionIcon[action.action_id];
                    return action;
                })
        },

        getMoreActions: function () {
            var self = this;
            return this.headerActions
                .filter(function (action) {
                    return !self.shownActionIcon[action.action_id];
                });
        }
    });

    return HeaderActions;
});