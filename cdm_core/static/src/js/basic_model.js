odoo.define('cdm_core.BasicModel', function (require) {
    "use strict";
    var BasicModel = require('web.BasicModel');
    var session = require('web.session');

    BasicModel.include({
        actionArchiveByResIds: function (resIDs, parentID) {
            var self = this;
            var parent = this.localData[parentID];

            return this._rpc({
                model: parent.model,
                method: 'action_archive',
                args: [resIDs]
            })
                .then(function (action) {
                    // optionally clear the DataManager's cache
                    self._invalidateCache(parent);
                    if (!_.isEmpty(action)) {
                        return self.do_action(action, {
                            on_close: function () {
                                return self.trigger_up('reload');
                            }
                        });
                    } else {
                        return self.reload(parentID);
                    }
                });
        },

        actionUnarchiveByResIds: function (resIDs, parentID) {
            var self = this;
            var parent = this.localData[parentID];

            return this._rpc({
                model: parent.model,
                method: 'action_unarchive',
                args: [resIDs],
            })
                .then(function (action) {
                    // optionally clear the DataManager's cache
                    self._invalidateCache(parent);
                    if (!_.isEmpty(action)) {
                        return self.do_action(action, {
                            on_close: function () {
                                return self.trigger_up('reload');
                            }
                        });
                    } else {
                        return self.reload(parentID);
                    }
                });
        },
        deleteRecordsByResIds: function (recordIds, allResIds, modelName) {
            var self = this;
            var records = _.map(recordIds, function (id) {
                return self.localData[id];
            });

            var context = _.extend(records[0].getContext(), session.user_context);
            return this._rpc({
                model: modelName,
                method: 'unlink',
                args: [allResIds],
                context: context,
            })
                .then(function () {
                    _.each(records, function (record) {
                        var parent = record.parentID && self.localData[record.parentID];
                        if (parent && parent.type === 'list') {
                            parent.data = _.without(parent.data, record.id);
                            delete self.localData[record.id];
                        } else {
                            record.res_ids.splice(record.offset, 1);
                            record.offset = Math.min(record.offset, record.res_ids.length - 1);
                            record.res_id = record.res_ids[record.offset];
                            record.count--;
                        }
                    });
                    // optionally clear the DataManager's cache
                    self._invalidateCache(records[0]);
                });
        },
    });
});