odoo.define('cdm_core.import_buttons', function (require) {
    "use strict";
    var ListView = require('web.ListView');
    var KanbanView = require('web.KanbanView');
    var ListController = require('web.ListController');
    var KanbanView = require('web.KanbanView');
    var KanbanController = require('web.KanbanController');

    require('base_import.import_buttons');

    var RestrictedActionMixin = {
        /**
         * @override
         * @param {Object} params
         * @param {boolean} [params.import_enabled=true] set to false to disable
         *   the Import feature (no 'Import' button in the control panel). Can also
         *   be disabled with 'import' attrs set to '0' in the arch.
         */
        init: function (viewInfo, params) {
            if (viewInfo.hasOwnProperty('restricted_actions')) {
                var restrictedActions = viewInfo.restricted_actions;
                this.controllerParams.restrictedActions = restrictedActions;
                this.controllerParams.bulkEnabled = Boolean(restrictedActions.bulk_action);
                this.controllerParams.activeActions.export_xlsx = Boolean(this.controllerParams.activeActions.export_xlsx && restrictedActions.export);
                this.controllerParams.importEnabled = Boolean(this.controllerParams.importEnabled && restrictedActions.import);
                return
            }
            this.restrictedActions = {};
            this.controllerParams.importEnabled = false;
            this.controllerParams.activeActions.export_xlsx = false;
        },
    };

    // Activate 'Import' feature on List views
    ListView.include({
        init: function () {
            this._super.apply(this, arguments);
            RestrictedActionMixin.init.apply(this, arguments);
        },
    });

    ListController.include({
        init: function (parent, model, renderer, params) {
            this._super.apply(this, arguments);
            this.bulkEnabled = params.bulkEnabled;
            this.restrictedActions = params.restrictedActions;
        },
    });

    // Activate 'Import' feature on List views
    KanbanView.include({
        init: function () {
            this._super.apply(this, arguments);
            RestrictedActionMixin.init.apply(this, arguments);
        },
    });

    KanbanController.include({
        init: function (parent, model, renderer, params) {
            this._super.apply(this, arguments);
            this.bulkEnabled = params.bulkEnabled;
            this.restrictedActions = params.restrictedActions;
        },
    })

});
