odoo.define('cdm_core.input_field', function (require) {
    "use strict";
    var InputField = require('web.basic_fields').InputField;

    InputField.include({
        _prepareInput: function ($input) {
            var result = this._super.apply(this, arguments);
            if (this.attrs.hasOwnProperty('field_type') && this.attrs.field_type === 'number') {
                result.keyup(function () {
                    var value = result.val();
                    value = value.replace(/\D/g, '');
                    result.val(value);
                })
            }

            return result;
        },
    });

});
