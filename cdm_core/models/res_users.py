# -*- coding: utf-8 -*-
import re
import logging

from odoo import models, fields, api, _, SUPERUSER_ID
from odoo.exceptions import UserError
from collections import defaultdict
from dateutil.relativedelta import relativedelta

from odoo import api, fields, models, _
from odoo.addons.auth_signup.models.res_partner import SignupError, now

_logger = logging.getLogger(__name__)

# Map to clear field if grouping not has groups and recompute field in another model if change role and references field
SCOPE_FIELD_GROUP_MAP = {}


class ResUsers(models.Model):
    _inherit = 'res.users'
    _groups_map = {}

    SELF_WRITEABLE_FIELDS = [
        'signature', 'action_id', 'company_id',
        'email', 'name', 'image', 'image_medium',
        'image_small', 'lang', 'tz',
        'login', 'email', 'phone', 'image_1920'
    ]

    def _default_groups(self):
        return self.env.ref('base.group_user')

    require_change_password = fields.Boolean(string=_("Require change password"),
                                             help="Force user to change password after login next time", default=True)
    grouping_id = fields.Many2one(string=_("Grouping of groups"), comodel_name='cdm_core.grouping')
    groups_id = fields.Many2many('res.groups', 'res_groups_users_rel', 'uid', 'gid', string='Groups', default=_default_groups)
    allow_edit_name = fields.Boolean(compute='_compute_edit_field')
    allow_edit_email = fields.Boolean(compute='_compute_edit_field')

    def _compute_edit_field(self):
        config = self.env['ir.config_parameter'].sudo()
        allow_edit_name = bool(config.get_param('cdm_core.user_allow_edit_name'))
        allow_edit_email = bool(config.get_param('cdm_core.user_allow_edit_email'))
        for record in self:
            record.allow_edit_name = allow_edit_name
            record.allow_edit_email = allow_edit_email

    def get_groups_map(self):
        if not self._groups_map:
            self._cr.execute("""SELECT data.module, data.name, data.res_id 
            FROM ir_model_data data WHERE model = 'res.groups'""")
            for module, name, id in self._cr.fetchall():
                self._groups_map['{}.{}'.format(module, name)] = id
        return self._groups_map

    def get_scope_field_groups_map(self):
        return SCOPE_FIELD_GROUP_MAP

    def get_group_scope_field_map(self):
        group_scope_field_map = {}
        for k, val in self.get_scope_field_groups_map().items():
            if k in self._fields:
                for g in val.get('groups', []):
                    if g not in group_scope_field_map:
                        group_scope_field_map[g] = []
                    group_scope_field_map[g].append(k)
        return group_scope_field_map

    def in_groups(self, groups_name, match_all=False):
        """
        Check user in groups or not
        :param groups_name: string or list, tuple of groups_name
        :param match_all: True: user must in all groups, False: user is member of one of groups
        :return: boolean
        """
        self.ensure_one()
        if not groups_name:
            return False
        groups_map = self.get_groups_map()
        groups_ids = self.groups_id.ids
        if isinstance(groups_name, str):
            return groups_map.get(groups_name) in groups_ids
        if isinstance(groups_name, list) or isinstance(groups_name, tuple):
            if match_all:
                groups_set = set()
                for gname in groups_name:
                    groups_set.add(groups_map.get(gname))
                return groups_set.issubset(set(groups_ids))
            else:
                for gname in groups_name:
                    if groups_map.get(gname) in groups_ids:
                        return True
                return False

    def check_access_rule(self, operation):
        current_user = self.env.user
        group_system_access = current_user.in_groups('base.group_system') or current_user.id == SUPERUSER_ID
        if not group_system_access:
            for record in self:
                if record.in_groups('base.group_system'):
                    raise UserError(_("You cannot edit user has setting role or assign setting role to this user"))
        return super(ResUsers, self).check_access_rule(operation)

    @api.model
    def signup(self, values, token=None):
        if token:
            # signup with a token: find the corresponding partner id
            partner = self.env['res.partner']._signup_retrieve_partner(token, check_validity=True, raise_exception=True)
            if partner.signup_type == 'reset':
                return super(ResUsers, self.with_user(SUPERUSER_ID)).signup(values, token)
        return super(ResUsers, self).signup(values, token)

    @api.constrains('login')
    def validate_login(self):
        for record in self:
            if record.login:
                match = re.match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$', record.login)
                if match == None:
                    raise models.ValidationError(_('Wrong email format!'))

    @api.model
    def change_password(self, old_passwd, new_passwd):
        result = super(ResUsers, self).change_password(old_passwd, new_passwd)

        if old_passwd == new_passwd:
            raise models.ValidationError(_('Old password and new password must be different'))

        # reset require_change_password value if change password success
        result and self.env.user.write({'require_change_password': False})

        return result

    def preference_change_password(self):
        action = super(ResUsers, self).preference_change_password()
        action['name'] = _('Change Password')

        return action

    @classmethod
    def authenticate(cls, db, login, password, user_agent_env):
        if login and type(login) == str:
            login = login.strip()

        return super(ResUsers, cls).authenticate(db, login, password, user_agent_env)

    # Custom signup email template
    def action_reset_password(self):
        """ create signup token for each user, and send their signup url by email """
        # prepare reset password signup
        create_mode = bool(self.env.context.get('create_user'))

        # no time limit for initial invitation, only for reset password
        expiration = False if create_mode else now(days=+1)

        self.mapped('partner_id').signup_prepare(signup_type="reset", expiration=expiration)

        # send email to users with their signup url
        template = False
        if create_mode:
            try:
                template = self.env.ref('cdm_core.set_password_email', raise_if_not_found=False)
            except ValueError:
                pass
        if not template:
            template = self.env.ref('cdm_core.reset_password_email')
        assert template._name == 'mail.template'

        template_values = {
            'email_to': '${object.email|safe}',
            'email_cc': False,
            'auto_delete': True,
            'partner_to': False,
            'scheduled_date': False,
        }
        template.write(template_values)

        for user in self:
            if not user.email:
                raise UserError(_("Cannot send email: user %s has no email address.") % user.name)
            with self.env.cr.savepoint():
                template.with_context(lang=user.lang).send_mail(user.id, force_send=True, raise_exception=True)
            _logger.info("Password reset email sent for user <%s> to <%s>", user.login, user.email)

    def send_unregistered_user_reminder(self, after_days=5):
        datetime_min = fields.Datetime.today() - relativedelta(days=after_days)
        datetime_max = datetime_min + relativedelta(hours=23, minutes=59, seconds=59)

        res_users_with_details = self.env['res.users'].search_read([
            ('share', '=', False),
            ('create_uid.email', '!=', False),
            ('create_date', '>=', datetime_min),
            ('create_date', '<=', datetime_max),
            ('log_ids', '=', False)], ['create_uid', 'name', 'login'])

        # group by invited by
        invited_users = defaultdict(list)
        for user in res_users_with_details:
            invited_users[user.get('create_uid')[0]].append("%s (%s)" % (user.get('name'), user.get('login')))

        # For sending mail to all the invitors about their invited users
        for user in invited_users:
            template = self.env.ref('cdm_core.mail_template_data_unregistered_users').with_context(dbname=self._cr.dbname, invited_users=invited_users[user])
            template.send_mail(user, notif_layout='mail.mail_notification_light', force_send=False)


class ChangePasswordUser(models.TransientModel):
    """ A model to configure users in the change password wizard. """
    _inherit = 'change.password.user'

    def change_password_button(self):
        for line in self:
            if not line.new_passwd:
                raise UserError(_("Before clicking on 'Change Password', you have to write a new password."))
            line.user_id.write({'password': line.new_passwd, 'require_change_password': True})
        # don't keep temporary passwords in the database longer than necessary
        self.write({'new_passwd': False})


class ChangePasswordWizard(models.TransientModel):
    """ A wizard to manage the change of users' passwords. """
    _inherit = 'change.password.wizard'

    @api.model_create_multi
    @api.returns('self', lambda value: value.id)
    def create(self, vals_list):
        records = super(ChangePasswordWizard, self).create(vals_list)

        for one in records:
            for line in one.user_ids:
                if not line.new_passwd:
                    raise UserError(_("Before clicking on 'Change Password', you have to write a new password."))

        return records
