# -*- coding: utf-8 -*-
import re

from odoo import models, fields, api, _


class IrActionsActWindow(models.Model):
    _inherit = 'ir.actions.act_window'

    dynamic_action = fields.Boolean(_('Is Dynamic Action'))

    def read(self, fields=None, load='_classic_read'):
        result = super(IrActionsActWindow, self).read(fields, load)
        for index, action in enumerate(result):
            if action.get('dynamic_action'):
                backup_action = action.copy()
                try:
                    result[index] = self.env[action.get('res_model')].read_dynamic_action(action)
                except Exception as e:
                    # Restore backup action to prevent bad effect of failed action
                    result[index] = backup_action
        return result

