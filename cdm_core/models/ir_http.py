# -*- coding: utf-8 -*-

from odoo import models
from odoo.http import request


class Http(models.AbstractModel):
    _inherit = 'ir.http'

    def session_info(self):
        user = request.env.user

        result = super(Http, self).session_info()
        result['require_change_password'] = getattr(user, "require_change_password", False)

        return result
