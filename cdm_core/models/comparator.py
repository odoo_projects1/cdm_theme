# -*- coding: utf-8 -*-
import re
from datetime import datetime

from odoo import models, _
from .base import DATE_FORMAT, TIME_FORMAT


def gt(a, b):
    return a > b


def lt(a, b):
    return a < b


def gte(a, b):
    return a >= b


def lte(a, b):
    return a <= b


def equals(a, b):
    return a == b


def not_equals(a, b):
    return a != b


def contains(a, b):
    return b in a


def not_contains(a, b):
    return b not in a


def is_in(a, b):
    return a in b


def is_not_in(a, b):
    return a not in b


def is_empty(a, b=None):
    return not a


def is_not_empty(a, b=None):
    return bool(a)


def is_match(a, b):
    return bool(re.match(b, a))


COMPARE_NAME_MAP = {
    '>': _('Greater than'),
    '<': _('Less than'),
    '>=': _('Greater than or equals to'),
    '<=': _('Less than or equals to'),
    '=': _('Equals to'),
    '!=': _('Not equals to'),
    'contains': _('Contains'),
    'not_contains': _('Not contains'),
    'not_empty': _('Not empty'),
    'empty': _('Empty'),
    'in': _('In'),
    'not_in': _('Not in'),
    'regex': _('Matches regex')
}

COMPARE_METHOD_MAP = {
    '>': gt,
    '<': lt,
    '>=': gte,
    '<=': lte,
    '=': equals,
    '!=': not_equals,
    'contains': contains,
    'not_contains': not_contains,
    'in': is_in,
    'not_in': is_not_in,
    'empty': is_empty,
    'not_empty': is_not_empty,
    'regex': is_match,
}


def to_date(str):
    return datetime.strptime(str, DATE_FORMAT)


def to_time(str):
    return datetime.strptime(str, TIME_FORMAT)


COMPARE_MAP = {
    'char': {
        'name': _('Char'),
        'support_operator': ['=', '!=', 'contains', 'not_contains', 'not_empty', 'empty', 'in', 'not_in', 'regex'],
        'format_db_value': None,
        'format_value': None
    },
    'text': {
        'name': _('Text'),
        'support_operator': ['=', '!=', 'contains', 'not_contains', 'not_empty', 'empty', 'in', 'not_in', 'regex'],
        'format_db_value': None,
        'format_value': None
    },
    'integer': {
        'name': _('Integer'),
        'support_operator': ['>', '<', '>=', '<=', '=', '!=', 'not_empty', 'empty', 'in', 'not_in'],
        'format_db_value': int,
        'format_value': int
    },
    'float': {
        'name': _('Float'),
        'support_operator': ['>', '<', '>=', '<=', '=', '!=', 'not_empty', 'empty', 'in', 'not_in'],
        'format_db_value': float,
        'format_value': float
    },
    'boolean': {
        'name': _('Boolean'),
        'support_operator': ['=', '!='],
        'format_db_value': bool,
        'format_value': bool
    },
    'date': {
        'name': _('Date'),
        'support_operator': ['>', '<', '>=', '<=', '=', '!='],
        'format_db_value': None,
        'format_value': to_date
    },
    'datetime': {
        'name': _('Datetime'),
        'support_operator': ['>', '<', '>=', '<=', '=', '!='],
        'format_db_value': None,
        'format_value': to_time
    },
    'many2one': {
        'name': _('Many2one'),
        'support_operator': ['=', '!=', 'not_empty', 'empty', 'in', 'not_in'],
        'format_db_value': None,
        'format_value': None
    },
    'many2many': {
        'name': _('Many2many'),
        'support_operator': ['contains', 'not_contains', 'not_empty', 'empty'],
        'format_db_value': None,
        'format_value': None
    },
}


class Comparator(models.AbstractModel):
    _name = 'cdm_core.comparator'
    _description = 'Compare value - value'
    """
    a operator b
    """

    @staticmethod
    def validate_compare(b, operator, field_type, field_name):
        type_info = COMPARE_MAP.get(field_type)
        if not type_info:
            raise models.ValidationError(_('Unknown field type'))

        support_operator = type_info.get('support_operator', [])
        if operator not in support_operator:
            operator_list = ', '.join(COMPARE_NAME_MAP.get(op) for op in support_operator)
            raise models.ValidationError(
                _('Field %s with type (%s) only support %s') % (field_name, type_info.get('name'), operator_list))

        if field_type == 'integer' and operator in ['>', '<', '>=', '<=', '=', '!=']:
            try:
                int(b)
            except Exception:
                operator_name = COMPARE_NAME_MAP.get(operator)
                raise models.ValidationError(
                    _('Please fill integer to compare value of field %s while using operator %s') % (
                        field_name, operator_name))
        elif field_type == 'float' and operator in ['>', '<', '>=', '<=', '=', '!=']:
            try:
                int(b)
            except Exception:
                operator_name = COMPARE_NAME_MAP.get(operator)
                raise models.ValidationError(
                    _('Please fill integer or float to compare value of field %s while using operator %s') % (
                        field_name, operator_name))
        elif field_type == 'date':
            try:
                to_date(b)
            except Exception:
                raise models.ValidationError(
                    _('Please leave fill date in format %s to compare value of field %s') % (DATE_FORMAT, field_name))
        elif field_type == 'datetime':
            try:
                to_date(b)
            except Exception:
                raise models.ValidationError(
                    _('Please leave fill datetime in format % to compare value of field %s') % (
                        TIME_FORMAT, field_name))

        return True

    @staticmethod
    def compare(a, b, operator, field_type):
        type_info = COMPARE_MAP.get(field_type)
        if type_info.get('format_db_value'):
            a = type_info['format_db_value'](a)

        if operator in ['in', 'not_in']:
            if type_info.get('format_value'):
                b = list(type_info['format_value'](x.strip()) for x in b.split(','))
            else:
                b = list(x.strip() for x in b.split(','))
        else:
            if type_info.get('format_value'):
                b = type_info['format_value'](b)

        return COMPARE_METHOD_MAP.get(operator)(a, b)
