from odoo import models, fields, _, api


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    user_allow_edit_name = fields.Boolean(_('Allow Edit Name'),
                                          config_parameter='cdm_core.user_allow_edit_name')
    user_allow_edit_email = fields.Boolean(_('Allow Edit Email'),
                                           config_parameter='cdm_core.user_allow_edit_email')
