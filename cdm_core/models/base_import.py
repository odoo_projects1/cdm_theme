# -*- coding: utf-8 -*-

from odoo import models, fields, api, _


class Import(models.TransientModel):
    _inherit = 'base_import.import'

    def check_access_rights(self, operation, raise_exception=True):
        if not self.env.user.has_group('cdm_core.group_import_action'):
            raise models.AccessError(_('You are not allowed to import data'))
        return super(Import, self).check_access_rights(operation, raise_exception)
