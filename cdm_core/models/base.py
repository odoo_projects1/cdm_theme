# -*- coding: utf-8 -*-
import re

from dateutil import tz
from psycopg2 import IntegrityError, errorcodes

from odoo import models, api, _, exceptions
from odoo.http import request

DATE_FORMAT = '%d/%m/%Y'
TIME_FORMAT = '%d/%m/%Y %H:%M:%S'


class Base(models.AbstractModel):
    _inherit = 'base'
    _handle_xml_views = []

    @staticmethod
    def get_cdm_date_format():
        return DATE_FORMAT

    @staticmethod
    def get_cdm_datetime_format():
        return TIME_FORMAT

    @staticmethod
    def cdm_date_strftime(d):
        return d.strftime(DATE_FORMAT)

    def cdm_time_strftime(self, d, with_user_tz=True):
        if with_user_tz:
            user_tz = tz.gettz(self.env.user.tz)
            return d.astimezone(user_tz).strftime(TIME_FORMAT)
        return d.strftime(TIME_FORMAT)

    def get_model_name(self):
        return self._name

    def get_model_id(self):
        return self.env['ir.model'].search([('model', '=', self._name)]).id

    def _get_remote_ip(self):
        return request.httprequest.environ['REMOTE_ADDR'] if request else 'n/a'

    def _handle_dynamic_xml(self, view_type, xml_content):
        """
        Replace xml content with custom content from dynamic method
        :param view_type:
        :param xml_content:
        :return: xml_content
        """
        if self._handle_xml_views and view_type in self._handle_xml_views:
            results = re.findall(r'<!-- cdm-replace-view-method="(.*)" -->', xml_content)
            for result in results:
                render_result = getattr(self, result)()
                replace_context = '<!-- cdm-replace-view-method="%s" -->' % result
                xml_content = xml_content.replace(replace_context, render_result)

        return xml_content

    @api.model
    def _fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        """
        Insert dynamic xml to view
        :param view_id:
        :param view_type:
        :param toolbar:
        :param submenu:
        :return:
        """
        result = super(Base, self)._fields_view_get(view_id=view_id, view_type=view_type,
                                                    toolbar=toolbar, submenu=submenu)
        result['arch'] = self._handle_dynamic_xml(view_type=view_type, xml_content=result.get('arch', ''))
        return result

    def export_data(self, fields_to_export):
        """
        Overwrite to check role before export
        """
        if not self.env.user.has_group('cdm_core.group_export_action'):
            raise models.AccessError(_('You are not allowed to export data'))
        return super(Base, self).export_data(fields_to_export)

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        result = super(Base, self).fields_view_get(view_id, view_type, toolbar, submenu)
        if view_type in ['tree', 'kanban']:
            user = self.env.user
            result['restricted_actions'] = {
                'import': user.in_groups('cdm_core.group_import_action'),
                'export': user.in_groups('cdm_core.group_export_action'),
                'bulk_action': user.in_groups('cdm_core.group_bulk_action')
            }
        return result

    def read_dynamic_action(self, action_result):
        """
        Return result for dynamic action
        :param domain:
        :return:
        """
        return action_result

    def unlink(self):
        try:
            return super(Base, self).unlink()
        except IntegrityError as inst:
            if inst.pgcode == errorcodes.FOREIGN_KEY_VIOLATION:
                msg = _('Cannot delete object is being used.')
                if 'active' in self._fields:
                    msg += ' ' + _('Please archive it instead.')

                raise exceptions.ValidationError(msg)
            else:
                raise inst

    @api.model
    def check_access_rights_all(self):
        operations = ['read', 'write', 'create', 'unlink']

        return {
            operation: self.check_access_rights(operation, raise_exception=False)
            for operation in operations
        }



