# -*- coding: utf-8 -*-
import re

from odoo import models, fields, api, _


class CdmValidator(models.AbstractModel):
    _name = 'cdm_core.validator'
    _description = 'Validate Utilities'

    @staticmethod
    def validate_tech_string(str, name):
        # at least one letter, a-z,_,0-9
        regex = r'^([a-z]|_|[0-9])*$'
        if not re.match(regex, str):
            raise models.ValidationError(_('All letter of %s must in set a-z,_,0-9') % name)

    @staticmethod
    def validate_tech_name(str, name, min_length=4, max_length=64):
        # at least one letter, a-z,_,0-9
        regex = r'^[a-z]([a-z]|_|[0-9]){%d,%d}$' % (min_length - 1, max_length - 1)
        if not re.match(regex, str):
            raise models.ValidationError(
                _('%s must be from %d to %d letter in set a-z,_,0-9 and not start with number') % (
                    name, min_length, max_length))
