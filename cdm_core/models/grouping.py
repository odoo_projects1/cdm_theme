# -*- coding: utf-8 -*-

from odoo import models, fields, api, _


class Grouping(models.Model):
    _name = 'cdm_core.grouping'
    _description = 'Grouping odoo groups to abstract groups'
    _order = 'create_date desc'

    name = fields.Char(string=_('Grouping name'), required=True)
    group_ids = fields.Many2many(string=_('Groups'), relation='loto_grouping_group_rel',
                                 comodel_name='res.groups', domain=lambda self: self.get_group_ids_domain(),
                                 column1='grouping_id', column2='group_id')
    all_group_ids = fields.Many2many(string=_('All Groups'), relation='loto_grouping_group_rel',
                                     comodel_name='res.groups', compute='compute_all_group_ids',
                                     column1='grouping_id', column2='group_id')
    user_ids = fields.One2many(string='Users', comodel_name='res.users',
                               inverse_name='grouping_id')

    _sql_constraints = [('name_unique', 'UNIQUE(name)', _('Grouping name has already existed'))]

    def write(self, vals):
        result = super(Grouping, self).write(vals)
        for record in self:
            grouping_users = self.env['res.users'].search([('grouping_id', '=', record.id)])
            grouping_users.apply_grouping_and_scope()
        return result

    def get_group_ids_domain(self):
        user = self.env.user
        if not user.user_has_groups('base.group_system'):
            all_loto_groups_data = self.env['ir.model.data'].search(
                [('model', '=', 'res.groups'), ('module', 'in', ['loto', 'loto_report'])])
            all_loto_groups_ids = list(r.res_id for r in all_loto_groups_data)
            return [('id', 'in', all_loto_groups_ids)]
        else:
            return []

    @api.depends('group_ids')
    def compute_all_group_ids(self):
        for record in self:
            all_gids = record.group_ids.ids
            for group in record.group_ids:
                all_gids.extend(group.trans_implied_ids.ids)
            record.all_group_ids = self.env['res.groups'].browse(set(all_gids))
