# -*- coding: utf-8 -*-
#############################################################################
#
#    CDM Technologies .,Ltd
#
#    Copyright (C) 2019-TODAY CDM Technologies(<https://www.cdmteck.com>)
#    Author: CDM Technologies(<https://www.cdmteck.com>)
#
#    You can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
#############################################################################

{
    'name': 'CDM Core',
    'author': "CDM Technologies",
    'version': '1.0',
    'category': 'CDM',
    'description': """
                    CDM core, include reusable resources and utilities for all CDM's module
                   """,
    'depends': [
        'base', 'base_setup', 'auth_signup'
    ],
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'views/assets.xml',
        'views/res_config_settings_views.xml',
        'views/res_users.xml',
        'views/menu.xml',
        'data/partner_root.xml',
        'data/auth_signup_data.xml',
    ],
    'qweb': [
        'static/src/xml/header_actions.xml',
        'static/src/xml/base.xml',
    ],
    'installable': True,
}
