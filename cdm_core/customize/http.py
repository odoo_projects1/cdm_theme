from odoo.http import JsonRequest, Response

old_json_response = JsonRequest._json_response

def new_json_response(self, result=None, error=None):
    if type(result) == Response:
        return result

    return old_json_response(self, result, error)

JsonRequest._json_response = new_json_response
